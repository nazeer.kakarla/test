#############
Upgrading OSD
#############


.. note::

	Let’s see how we can upgrade the stack. Opensearch and Opensearch-Dashboards are the two things you need to be familiar with. In this article where OSD is mentioned it will mean the Gitlab repository version of Opensearch-Dashboards & 4px.osd2 means Customized OSD i.e. 4PointX.


.. raw:: html

	<ol start="1"><li><p>Clone <code>4px.osd2</code> and modify run.sh to clone and apply customization to new OSD version.<br/>Example: upgrading to 1.3.6</p><ac:structured-macro ac:macro-id="351f7935-af15-4fe4-a4e6-e7c8e8f24339" ac:name="code" ac:schema-version="1"><ac:parameter ac:name="language">bash</ac:parameter><ac:plain-text-body><![CDATA[#!/bin/sh
cd `dirname "$0"`
rm -rf OpenSearch-Dashboards

# cloning OSD repo
git clone -b 1.3.6 git@gitlab.com:everlytics/OpenSearch-Dashboards.git

# running python code to copy files to their location
python3 copy_customization_files.py &&

# building OSD with plugins
cd OpenSearch-Dashboards]]></ac:plain-text-body></ac:structured-macro></li><li><p>Copy plugins and add updated stock plugins.</p></li><li><p>Update the package.json to reflect the upgraded packages and info as found inside the OSD.</p></li><li><p>Use <code>docker run -p 9200:9200 -p 9600:9600 -e "discovery.type=single-node" opensearchproject/opensearch:1.3.6</code> to run Opensearch Container. More info <a href="https://opensearch.org/versions/opensearch-1-3-6.html">here</a>. It will help you run and test the workings of new 4px.osd2 in local env. Modify <code>config/opensearch-dashboards.yml</code> to point to <code>localhost:9200</code> with username and password as <code>admin</code>.</p></li><li><p>Now start the bootstrap process and resolve the errors. If <code>src</code> files require updates, make sure to place the respective file and its path in <code>customization</code> folder and update the <code>config.py</code></p></li><li><p>Inside <code>plugins/samplePlugin/opensearch_dashboards.json</code> of each plugin, update the respective version here.</p><ac:structured-macro ac:macro-id="1e758108-0341-4665-acc6-dae5b0679f76" ac:name="code" ac:schema-version="1"><ac:plain-text-body><![CDATA[  "opensearchDashboardsVersion": "1.3.6",]]></ac:plain-text-body></ac:structured-macro></li><li><p>Make sure to update the security-dashboards-plugin in order to have the custom login page. </p></li><li><p>Lastly Put everything from <code>Opensearch-Dashboard</code> directory to the <code>4px.osd2</code> and do not replace everything, you need to make sure 4px.osd2 still has those custom updated files added by Everlytics for development/cicd purposes. Make commit and push.</p></li></ol>

.. toctree::
