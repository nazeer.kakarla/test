##################################
Create custom monitor for alerting
##################################




.. raw:: html

	<p>Monitor your data and send alerts based on custom requirements - Like check Speed limit !&gt; 100 on Machine A , shop B , plant C. </p>

.. raw:: html

	<p>Unlike in a simple monitor , here you can filter data to point to the exact rows which need to be monitored. Using a DSL query json, data is first fetched and then checked to see if the condition specified is met. </p>

.. raw:: html

	<p>Rest of the process is the same.</p>

************
Instructions
************


Set Up Your Destination
=======================

.. raw:: html

	<p>When you create alerts in Alerting plugin, you create one or many monitors and one or many Destinations. You can use your Slack channel or you can set up a custom webhook (a URL) to receive messages. If you choose a custom webhook, you will set up headers and a message body, and the plugin will POST its message to the destination URL.</p>



.. raw:: html

	<ol start="1"><li><p>Open Kibana in your browser and click the Alerting tab.</p></li><li><p>At the top of the window, click Destinations, then click Add Destination.</p><ac:image ac:align="center" ac:layout="center" ac:original-height="302" ac:original-width="758"><ri:attachment ri:filename="0DjHnuevNjNzFR6JOBd6OjDkvNmvcEVzw9OfQk0TfHD6vdZuu3FH67xK0437cEgoc7k_BY-gpqRanDVZLPNC519rDc3xsjkjvB1gtmkxHiYjfArcs642FW-SUFGiV_t2khtg_KKO" ri:version-at-save="1"></ri:attachment></ac:image></li></ol>

.. raw:: html

	<p>3. In the Add Destination dialog, give your destination a Name, choose the Type of destination, and set the Webhook URL. For eg: I have selected Custom Webhook and given the name SMS. </p>

.. raw:: html

	<img src='/docs/_static/media/pzWlfothkr3boEE1Fi9NlswNcHWjBAYGeFG2uzeHUoUmMzXdGqNewTuqzEihdnNG6IGwRfXtIi343_P9blnIFw0aVQm2NGDVw_hZNqTX5_3YZM5tvhikzKebY4tW2P0Rjc-JgQSD'>

.. raw:: html

	<p>4. Click Create. </p>



.. raw:: html

	<p> </p>


Set Up a Monitor
================

.. raw:: html

	<p>Monitors allow you to specify a value that you want to monitor. You can select the value either graphically or by specifying an Elasticsearch query. You define a monitor first and then define triggers for the value that you are monitoring.</p>

.. raw:: html

	<ol start="1"><li><p>Click the Monitors tab and then Create Monitor.</p></li><li><p>Select  - “Define using extraction query”</p></li></ol>

.. raw:: html

	<img src='/docs/_static/media/image-20200327-141235.png'>

.. raw:: html

	<p>3. Copy paste this json for the usecase - a. Alert when Plant &lt;A&gt; , Shop &lt;B&gt; , Machine &lt;C&gt; field &lt;D&gt; avg exceeds certain &lt;limit&gt;. Change values as applicable where ever ** is marked .</p>

.. raw:: html

	<p><code>"body": {</code><br/> <code>"size": 10,</code><br/><code>"query": {</code><br/><code>"bool": {</code><br/><code>"must": [],</code><br/><code>"filter": [</code><br/><code>{</code><br/><code>"bool": {</code><br/><code>"should": [</code><br/><code>{</code><br/><code>"range": {</code><br/><code>"cumulative_energy": {</code> **<br/><code>"gte": 25 **</code><br/><code>}</code><br/><code>}</code><br/><code>}</code><br/><code>],</code><br/><code>"minimum_should_match": 1</code><br/><code>}</code><br/><code>},</code><br/><code>{</code><br/><code>"match_phrase": {</code><br/><code>"dept": {</code><br/><code>"query": "TWP" **</code><br/><code>}</code><br/><code>}</code><br/><code>},</code><br/><code>{</code><br/><code>"match_phrase": {</code><br/><code>"sub_dept": {</code><br/><code>"query": "TWP" **</code><br/><code>}</code><br/><code>}</code><br/><code>},</code><br/><code>{</code><br/><code>"range": {</code><br/><code>"eventTimeStamp": {</code><br/><code>"gte": "now-5y",</code><br/><code>"lte": "now"</code><br/><code>}</code><br/><code>}</code><br/><code>}</code><br/><code>],</code><br/><code>"should": [],</code><br/><code>"must_not": []</code><br/><code>}</code><br/><code>}</code><br/><code>}</code></p>

.. raw:: html

	<p>Click Create. This will bring you to the Define Trigger page.</p>


Create a Trigger
================

.. raw:: html

	<p>When you create a trigger, you specify the threshold value for the field you’re monitoring. When the value of the field exceeds the threshold, the Monitor enters the Active state.</p>

.. raw:: html

	<ol start="1"><li><p>Give your Trigger condition a name. For example, CPU Too High.</p></li><li><p>Select the severity level for the trigger condition.</p></li><li><p>Select the &lt;IS ABOVE&gt; | &lt;IS BELOW&gt; | &lt;IS EXACTLY&gt; expression and change the value to &lt;??&gt; for triggering an alert. As you change the condition, the visualization is automatically updated.</p><p>For example, here I have selected the IS ABOVE condition as 90 and severity level as 1.</p></li></ol>

.. raw:: html

	<img src='/docs/_static/media/utZrrEy2Ya4Vysh_QGBxehIbBCXXsPaC9WZ-y-Na9NFB8Vrisd-h7AStSuRMFXf6QfOu9TDogIxIU2JLNdtbk88S3Z68GWIAdmuhq-TmEN7jXf4D5y1TMm-ydk5uIEicDwHL1mCe'>

.. raw:: html

	<p>4.Once you’ve set the trigger conditions, you set the action or actions that the alerting performs.</p>


Configure Actions
=================

.. raw:: html

	<ol start="1"><li><p>Give your Alert action a name. </p></li><li><p>Select the Destination.</p></li><li><p>Once you have set the action, Click Create.</p></li></ol>

.. raw:: html

	<img src='/docs/_static/media/kgPdFfRqlomHTQCIEeUJHdKju7ojbHvL6ePHgXHjOMHUzNlMu0vVyUVV8_dNjc0f526u6QAvBQjSc5kPRM1Zi181176v326FiBWfbm6e0Qq4xFtbQ5k1218zE6RchYXWubjXQNwL'>

.. raw:: html

	<p>The action triggers when the trigger condition is met.</p>

.. toctree::
