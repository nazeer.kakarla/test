####################
Real-time Monitoring
####################





Grouping Interval logic implemented in the Front-End:
=====================================================

.. raw:: html

	<p>We are using a custom function <code>calculateInterval</code> which is responsible for determining the appropriate time interval based on a given number of hours. It rounds the input hours to the nearest integer compares the rounded value with predefined maximum hour thresholds and dispatches the selected interval to the Redux store. If no match is found it returns a default value ('1d' for 1 day).</p>


Code:
=====

.. code-block:: text

	 /**
	   * Calculates the appropriate time interval based on the given number of hours.
	   *
	   * This function takes the number of hours as input and rounds it to the nearest integer.
	   * It then compares the rounded hours with predefined maximum hour thresholds to determine
	   * the suitable time interval. If a match is found it dispatches the interval to the Redux store
	   * it returns the selected interval or a default value ('1d' for 1 day).
	   *
	   * @param {number} hours - The number of hours to calculate the interval for.
	   * @returns {string} - The calculated time interval.
	   */
	  const calculateInterval = useCallback((hours) => {
	    const roundedHours = Math.round(hours);
	
	    if (isNaN(roundedHours) || roundedHours === 0) {
	      return '1s';
	    }
	
	    const intervalChoices = [
	      { maxHours: 0, interval: '1s' },
	      { maxHours: 1, interval: '1s' },
	      { maxHours: 2, interval: '2s' },
	      { maxHours: 3, interval: '5s' },
	      { maxHours: 4, interval: '10s' },
	      { maxHours: 8, interval: '20s' },
	      { maxHours: 12, interval: '30s' },
	      { maxHours: 24, interval: '1m' },
	      { maxHours: 48, interval: '2m' },
	      { maxHours: 120, interval: '5m' },
	      { maxHours: 240, interval: '10m' },
	      { maxHours: 480, interval: '30m' },
	      { maxHours: 720, interval: '1h' },
	      { maxHours: 1440, interval: '2h' },
	      { maxHours: 2160, interval: '5h' },
	      { maxHours: 4320, interval: '10h' },
	      { maxHours: 6480, interval: '12h' },
	      { maxHours: 8640, interval: '1d' },
	    ];
	
	    for (const { maxHours, interval } of intervalChoices) {
	      if (roundedHours <= maxHours && roundedHours > maxHours / 2) {
	        return interval;
	      }
	    }
	
	    return '1d';
	  }, []);


.. raw:: html

	<p><br/></p>



.. raw:: html

	<p>The <code>parseTimeString</code> function is designed to parse a time string and produce a formatted ISO string representing the calculated time. It supports a variety of time units (seconds, minutes, hours, days, weeks, months, years) and can handle both absolute and relative time expressions.</p>

.. raw:: html

	<p>We are not using <code>dateMath.parse(time)</code> due to inconsistencies with unparsable strings, where the behavior can vary between returning <code>undefined</code> and providing an invalid moment.</p>


Code:
=====

.. code-block:: text

	/**
	   * Parses a time string and returns a formatted ISO string representing the calculated time.
	   *
	   * This function takes a time string as input and processes it to determine the corresponding
	   * time based on the provided format. It supports various units (seconds, minutes, hours, days,
	   * weeks, months, years) and can handle relative time expressions like 'now-5d' (5 days ago) or
	   * 'now+3h' (3 hours from now). If 'now' is provided, it returns the current time in ISO format.
	   *
	   * @param {string} timeString - The time string to be parsed (e.g., 'now-5d', 'now+3h').
	   * @returns {string} - The formatted ISO string representing the calculated time.
	   */
	  const parseTimeString = (timeString) => {
	    if (timeString === 'now') {
	      // Return the current time if 'now' is provided
	      return moment().toISOString();
	    }
	
	    const now = moment();
	
	    const regex = /now([+-])(\d+)([smhdwMy])/;
	    const match = regex.exec(timeString);
	
	    if (match) {
	      const operator = match[1];
	      const value = parseInt(match[2], 10);
	      const unit = match[3];
	
	      switch (unit) {
	        case 's':
	          return operator === '+'
	            ? now.add(value, 'seconds').toISOString()
	            : now.subtract(value, 'seconds').toISOString();
	        case 'm':
	          return operator === '+'
	            ? now.add(value, 'minutes').toISOString()
	            : now.subtract(value, 'minutes').toISOString();
	        case 'h':
	          return operator === '+'
	            ? now.add(value, 'hours').toISOString()
	            : now.subtract(value, 'hours').toISOString();
	        case 'd':
	          return operator === '+'
	            ? now.add(value, 'days').toISOString()
	            : now.subtract(value, 'days').toISOString();
	        case 'w':
	          return operator === '+'
	            ? now.add(value, 'weeks').toISOString()
	            : now.subtract(value, 'weeks').toISOString();
	        case 'M':
	          return operator === '+'
	            ? now.add(value, 'months').toISOString()
	            : now.subtract(value, 'months').toISOString();
	        case 'y':
	          return operator === '+'
	            ? now.add(value, 'years').toISOString()
	            : now.subtract(value, 'years').toISOString();
	        default:
	          // Handle other units as needed
	          return now.toISOString();
	      }
	    }
	
	    return now.toISOString();
	  };


.. toctree::
