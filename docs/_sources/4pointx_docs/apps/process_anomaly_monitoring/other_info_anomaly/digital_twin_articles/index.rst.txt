#####################
Digital Twin articles
#####################




.. raw:: html

	<p><a data-card-appearance="inline" href="https://new.siemens.com/global/en/markets/automotive-manufacturing/digital-twin-performance.html">https://new.siemens.com/global/en/markets/automotive-manufacturing/digital-twin-performance.html</a> </p>

.. raw:: html

	<p><a data-card-appearance="inline" href="https://www.biz4intellia.com/blog/rise-of-digital-twin-in-manufacturing-sector/">https://www.biz4intellia.com/blog/rise-of-digital-twin-in-manufacturing-sector/</a> </p>

.. raw:: html

	<p><a data-card-appearance="inline" href="https://www.perforce.com/blog/vcs/digital-twin-manufacturing">https://www.perforce.com/blog/vcs/digital-twin-manufacturing</a> </p>

.. toctree::
