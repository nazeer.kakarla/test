###########################
Deploy 4PointX Self-Managed
###########################


.. toctree::

	pre_requisites/index
	pre_installation_guide/index
	deploy_4pointx_iaot_suite/index
	log_in_to_4pointx_iaot_suite/index
	upgrade_or_uninstall_4pointx/index
	maintain_4px_cluster/index
	tls_security/index
	next_steps/index