#########################
Deploy 4PointX IAoT Suite
#########################


.. raw:: html

	<p><strong>Login to K8S Master VM and go to </strong><code>4px</code> Linux User<strong>:</strong></p>

.. code-block:: text

	sudo su - 4px


.. raw:: html

	<p>Now, Make the required changes to the <code>containerd.service</code> configuration. For, Kubernetes to access the docker registry.</p>

#.  Containerd Changes to connect with 4PX Image Repository
	#. Open containerd Configuration file:
		.. code-block:: text
		
			sudo vi /etc/containerd/config.toml
		
		
	#. Edit/Replace the configuration to access insecure-registry registry.4pointx.com:5000:
		.. code-block:: text
		
			[plugins."io.containerd.grpc.v1.cri".registry]
			      config_path = ""
			
			      [plugins."io.containerd.grpc.v1.cri".registry.auths]
			
			      [plugins."io.containerd.grpc.v1.cri".registry.configs]
			        [plugins."io.containerd.grpc.v1.cri".registry.configs."registry.4pointx.com:5000".tls]
			          insecure_skip_verify = true
			      [plugins."io.containerd.grpc.v1.cri".registry.headers]
			
			      [plugins."io.containerd.grpc.v1.cri".registry.mirrors]
			        [plugins."io.containerd.grpc.v1.cri".registry.mirrors."registry.4pointx.com:5000"]
			          endpoint = ["https://registry.4pointx.com:5000"]
		
		
	#. Restart containerd
		.. code-block:: text
		
			sudo systemctl restart containerd
		
		
	#. Now, Repeat the above 3 steps: a,b,c on all the Kubernetes nodes.
	
	
	
#. Run the following command on EACH WORKER NODE.Ref Link: OS Installation Important Settings Commands:
	.. code-block:: text
	
		sudo sysctl -w vm.max_map_count=1000000 
		sudo sysctl -p
	
	
#. Before deploying 4pointX, make sure there should be a ssl certificate. If not, will create manually using below command otherwise rename .crt into __4pointx_com.crt and .key as server.key



.. code-block:: text

	openssl req -x509 -nodes -days 365 -newkey rsa:2048 -subj "/C=IN/ST=KN/L=BN/O=EV/OU=4PX/CN=*.4pointx.com" -keyout server.key -out __4pointx_com.crt


#. Create secret using below command where pwd is present path.



.. code-block:: text

	kubectl create secret generic osd --from-file=/pwd/__4pointx_com.crt --from-file=/pwd/server.key -n 4px


.. raw:: html

	<p><strong>Create Namespace in K8S Cluster:</strong></p>

.. raw:: html

	<p>Run command from K8S Master Node (VM):</p>

.. code-block:: text

	kubectl create namespace 4px


.. raw:: html

	<p>Check whether the namespace is created by listing namespaces:</p>

.. code-block:: text

	kubectl get namespace


.. raw:: html

	<p><strong>Create Object Definition File</strong></p>

.. raw:: html

	<p><strong>4px_setup.yaml</strong></p>

.. code-block:: text

	apiVersion: apps/v1
	kind: Deployment
	metadata:
	  name: k8s-setup
	  namespace: 4px
	spec:
	  replicas: 1
	  selector:
	    matchLabels:
	      app: k8s-setup
	  template:
	    metadata:
	      labels:
	        app: k8s-setup
	    spec:
	      serviceAccount: k8s-setup
	      serviceAccountName: k8s-setup
	      containers:
	      - name: setup-k8s
	        image: registry.4pointx.com:5000/4pointx/setup:k8s
	        imagePullPolicy: Always
	        env:
	        - name: VERSION
	          value: "master"
	        - name: KAFKA_BROKER_DATA_VOLUME_SIZE
	          value: "4Gi"
	        - name: KAFKA_BROKER_LOGS_VOLUME_SIZE
	          value: "4Gi"
	        - name: KAFKA_BROKER_REPLICAS
	          value: "3"
	        #The number of nodeports should be equal to the number of replicas.
	        #The values should be separated by comma and in "".
	        - name: KAFKA_BROKER_NODEPORT
	          value: "32005, 32006, 32007"
	        - name: KAFKA_BROKER_HOST_NAME
	          value: "34.16.187.60"
	        - name: KAFKA_ZOOKEEPER_VOLUME_SIZE
	          value: "4Gi"
	        - name: KAFKA_ZOOKEEPER_REPLICAS
	          value: "1"
	        - name: OPENSEARCH_VOLUME_SIZE
	          value: "4Gi"
	        - name: OPENSEARCH_REPLICAS
	          value: "3"
	        - name: OPENSEARCH_HEAP_SIZE_MB
	          value: "4096"
	        - name: STORAGE_CLASS
	          value: "longhorn"
	        command: ["/bin/bash"]
	        args:
	        - "-c"
	        - "/app/run.sh"
	---
	apiVersion: v1
	kind: ServiceAccount
	metadata:
	  name: k8s-setup
	  namespace: 4px
	---
	apiVersion: rbac.authorization.k8s.io/v1
	kind: Role
	metadata:
	  name: k8s-setup-role
	  namespace: 4px
	rules:
	- apiGroups: ["*"]
	  resources: ["*"]
	  verbs: ["*"]
	---
	apiVersion: rbac.authorization.k8s.io/v1
	kind: RoleBinding
	metadata:
	  name: k8s-setup-role-binding
	  namespace: 4px
	roleRef:
	  apiGroup: rbac.authorization.k8s.io
	  kind: Role
	  name: k8s-setup-role
	subjects:
	- kind: ServiceAccount
	  name: k8s-setup
	  namespace: 4px
	---
	apiVersion: rbac.authorization.k8s.io/v1
	kind: ClusterRole
	metadata:
	  name: k8s-setup-cluster-role
	rules:
	- apiGroups: ["*"]
	  resources: ["*"]
	  verbs: ["*"]
	---
	apiVersion: rbac.authorization.k8s.io/v1
	kind: ClusterRoleBinding
	metadata:
	  name: k8s-setup-cluster-role-binding
	roleRef:
	  apiGroup: rbac.authorization.k8s.io
	  kind: ClusterRole
	  name: k8s-setup-cluster-role
	subjects:
	- kind: ServiceAccount
	  name: k8s-setup
	  namespace: 4px
	---
	apiVersion: v1
	kind: Service
	metadata:
	  name: k8s-setup
	  namespace: 4px
	spec:
	  selector:
	    app: k8s-setup
	  type: ClusterIP
	  ports:
	    - protocol: TCP
	      port: 80
	      targetPort: 8090


.. raw:: html

	<p><strong>Note: </strong>Change the Version and size of PV for the respective module.<br/></p>

.. raw:: html

	<p><strong>Commands:</strong></p>

.. raw:: html

	<p>Now, create a file called <code>4px_setup.yaml</code> and paste the above content to the file on the k8s master node. You will use <strong>kubectl </strong>to do the actual object creation and cluster deployment.</p>

#. Now, Apply the 4px_setup.yaml file using kubectl.
	.. code-block:: text
	
		kubectl  apply -f 4px_setup.yaml
	
	
#. Check the status of all modules.
	.. code-block:: text
	
		kubectl get pods -n 4px
	
	



.. raw:: html

	<img src='/docs/_static/media/9zJpCE3bQshF3wE8OnGfUZ1ZpE9Oq-PT36N_Jucbujf4GbxRnbT_zvi2fJuj9FzIZ7dWipcLdqlXRgUXp7SVB-olbDzBOkzV3pIqaiq7gSdthRaIIuHxzZRxWsgsTjJ84BdAv-wCiSoY-e6ZDqEhWqs'>

.. raw:: html

	<p><strong>Note: </strong>By default, there will be 2 Node <code>Kafka-broker</code> and <code>Opensearch</code>, 1 Node <code>Zookeeper</code> and <code>Opensearch-Dashboard</code>. To scale up and scale down  Refer To: the <code>Resize 4PX Modules</code> Page under the Administration Tab.</p>

#. Test 4px Web console



.. raw:: html

	<p style="margin-left: 30.0px;">Run the below command on the k8s master node to check if the web console is accessible:</p>

.. code-block:: text

	curl -vk https://localhost:32000


.. toctree::
