########################
Upgrade 4PointX Platform
########################


.. raw:: html

	<p>The version of the 4PointX Platform is to me updated <strong>manually. </strong>Edit the VERSION environment variable in deploy_setup.yaml file. For example you want the version to be “k8s“, the deploy_setup.yaml file will look like this:<br/></p>

.. raw:: html

	<img src='/docs/_static/media/image-20240214-092454.png'>

.. raw:: html

	<p>If you want to change it to version 1.1.0, replace k8s with 1.1.0 and run the following command.</p>

.. code-block:: text

	kubectl apply -f deploy_setup.yaml -n 4px


.. raw:: html

	<p>This will update the version of the entire platform and it’s components.</p>

.. toctree::
