##################
Resize 4PX Modules
##################


.. raw:: html

	<p>This guide will help you set the following things in your OpenSearch and Kafka pods:</p>

* Replica Count
* Storage Class



.. raw:: html

	<p><strong>Instructions:</strong></p>

.. raw:: html

	<p>There are two ways to change the number of replicas of Kafka, OpenSearch, or any module you want to change it for.</p>

* Scale up/down using k8s dashboard
* Scale up/down using deployment files



.. raw:: html

	<p><strong>Using k8s dashboard:</strong></p>

.. raw:: html

	<p>Prerequisite:<br/>K8s dashboard should be configured properly and accessible on https://&lt;IP of Master Node&gt;:30087. To install and configure the k8s dashboard, refer to the article “How to install and access the k8s dashboard“.</p>

.. raw:: html

	<p>Now once you can access the k8s dashboard:</p>

* To scale up/down kafka broker, kafka zookeeper or opensearch:



#. Navigate to StatefulSets from the left-hand pane and select the one you want to scale.



.. raw:: html

	<img src='/docs/_static/media/image-20231215-094452.png'>

#. Select Kafka broker(or any other you want to scale up or down) 



.. raw:: html

	<p>By default, there are 3 broker pods, 1 zookeeper, and 3 opensearch pods.</p>

.. raw:: html

	<img src='/docs/_static/media/image-20231215-094820.png'>

#. Click on the 3 dots icon in the top right corner(Scale Resource). Set the desired number of pods(4 in this case)



.. raw:: html

	<img src='/docs/_static/media/image-20231215-094946.png'>

#. Click on Scale and you can see that now you have 4 pods instead of 3. Similarly, you can scale Kafka Zookeeper and OpenSearch.



* To scale API modules like PDM, cm, alerting, and all:



#. Navigate to Deployments from the left side plane and select the one you want to scale.



.. raw:: html

	<img src='/docs/_static/media/image-20231215-095359.png'>

#. Select nginx-deployment(or any other you want to scale up and down). By default, there is one pod for all API modules.



.. raw:: html

	<img src='/docs/_static/media/image-20231215-095646.png'>

#. Now again, click on the three dots in the top right corner and scale to your desired number of pods as described above. 



.. raw:: html

	<p><strong>Using Deployment files:</strong></p>

#. Open Terminal and run the following command:(Make sure k8s is installed and you are on Master VM)



.. raw:: html

	<p>For kafka-broker, kafka-zookeeper and opensearch:</p>

.. code-block:: text

	kubectl scale -n <namespace> statefulset <name of pod> --replicas=<desired number of pods>


.. raw:: html

	<p>For API modules like config, alerting, etc.</p>

.. code-block:: text

	kubectl scale -n <namespace> deployment <name of pod> --replicas=<desired number of pods>


#. To specify the storage class 
	.. raw:: html
	
		<img src='/docs/_static/media/image-20231213-115605.png'>
	
	.. raw:: html
	
		<p>You can specify any storage class you have created and want to use this particular persistence.</p>
	



.. toctree::
