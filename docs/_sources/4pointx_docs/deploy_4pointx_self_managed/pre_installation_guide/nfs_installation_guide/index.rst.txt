######################
NFS Installation Guide
######################


.. raw:: html

	<p>This article is a guide on the installation process of nfs (Network File System) in your VMs. To install you will need at least two VMs one will serve as host and another one will be a client. Once nfs is installed, we can create storage classes out of it which will help in allocating PV and PVC later in the pods in k8s deployment. <br/></p>

.. raw:: html

	<p><strong>Steps: Follow steps to install NFS in VM for RHEL OS:</strong></p>

#. Run the following commands
	.. code-block:: text
	
		sudo yum install nfs-utils
	
	
#. Enable NFS service
	.. code-block:: text
	
		sudo systemctl enable nfs-server
		sudo systemctl start nfs-server
		sudo systemctl status nfs-server
	
	
#. Create a Shared Directory which will be available on all the clients.
	.. code-block:: text
	
		sudo mkdir /nfs
	
	
#. Change the ownership of /nfsdirectory.
	.. code-block:: text
	
		sudo chown nobody /nfs
	
	
#. Configure the NFS Exports:
	.. code-block:: text
	
		sudo vi /etc/exports
	
	
#. Add the directories you want to share. 
	.. code-block:: text
	
		/nfs <client-1_IP>(rw,sync,no_subtree_check) <client-2_IP>(rw,sync,no_subtree_check) <client-3_IP>(rw,sync,no_subtree_check) <client-4_IP>(rw,sync,no_subtree_check)
	
	
#. Save and apply changes.
	.. code-block:: text
	
		sudo exportfs -a
	
	
#. Configure firewalld service
	.. code-block:: text
	
		firewall-cmd --permanent --add-service=nfs
		firewall-cmd --permanent --add-service=mountd
		firewall-cmd --permanent --add-service=rpc-bind
		firewall-cmd --reload
	
	
	.. code-block:: text
	
		sudo firewall-cmd --permanent --list-all | grep services
	
	
	.. code-block:: text
	
		Output:
		  services: mountd nfs nfs3 rpc-bind
	
	



.. raw:: html

	<p><strong>Follow the given steps on the NFS client VM:</strong></p>

#. Run the following commands
	.. code-block:: text
	
		sudo yum install nfs-utils
	
	
#. Enable NFS service
	.. code-block:: text
	
		sudo systemctl enable nfs-server
		sudo systemctl start nfs-server
		sudo systemctl status nfs-server
	
	
#. Create a Shared Directory where you will be mounting the NFS directory.
	.. code-block:: text
	
		sudo mkdir -p /nfs
	
	
#. Mount directory
	.. code-block:: text
	
		sudo mount <NFS_HOST_IP>:/nfs /nfs
	
	
#. Edit fstab to make the nfs drive persistent over VM reboot.
	.. code-block:: text
	
		<NFS_HOST_IP>:/nfs /nfs nfs defaults 0 0
	
	



.. raw:: html

	<p><strong>References:</strong></p>

.. raw:: html

	<p>For RedHat Based Installation: <a data-card-appearance="inline" href="https://www.digitalocean.com/community/tutorials/how-to-set-up-an-nfs-mount-on-rocky-linux-9">https://www.digitalocean.com/community/tutorials/how-to-set-up-an-nfs-mount-on-rocky-linux-9</a> </p>

.. raw:: html

	<p>For Ubuntu Based Installation: <a data-card-appearance="inline" href="https://www.tecmint.com/install-nfs-server-on-ubuntu/">https://www.tecmint.com/install-nfs-server-on-ubuntu/</a> </p>

.. toctree::
