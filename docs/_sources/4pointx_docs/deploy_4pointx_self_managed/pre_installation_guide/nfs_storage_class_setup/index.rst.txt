#######################
NFS Storage Class Setup
#######################


.. raw:: html

	<p>Once NFS Server installed in VM, To Provision NFS Persistent<a href="https://kubernetes.io/docs/concepts/storage/volumes/#nfs"> Volumes</a> as Storage. Need to provision <code>storage class</code> out of the NFS which will later be specified while allocating Persistent Volumes (PV) and Persistent Volume Claim (PVC). Deploy <code>storage class</code> NFS External Provisioner using helm on Master Node K8S VM by running below steps.</p>

.. raw:: html

	<p><strong>Steps: </strong></p>

#. Add the NFS External Provisioner Helm repository.
	.. code-block:: text
	
		helm repo add nfs-subdir-external-provisioner https://kubernetes-sigs.github.io/nfs-subdir-external-provisioner/
	
	
#. Install the NFS External Provisioner Helm chart:
	.. code-block:: text
	
		helm install nfs-subdir-external-provisioner nfs-subdir-external-provisioner/nfs-subdir-external-provisioner \
		--set nfs.server=35.XX.XX.135 \
		--set nfs.path=/nfs \
		--set storageClass.name=standard \
		--set storageClass.allowVolumeExpansion=true
	
	
#. Now your storage class is up and ready to be used for creating PVs. (Make sure to remember the storageClass.name=standard you choose in the previous step)
	.. code-block:: text
	
		kubectl get storageclass -n <namespace>
	
	



.. raw:: html

	<p style="margin-left: 30.0px;"><br/>Use the above command to get information about your <code>storage class</code> details.</p>

#. After successfully setting up StorageClass you can proceed with 4px_setup.yaml deployment.



.. raw:: html

	<p><strong>Resizing PV size for modules(Kafka-broker, Kafka-zookeeper, and Opensearch):</strong></p>

#. Login To Kubernetes Dashboard
	.. raw:: html
	
		<img src='/docs/_static/media/image-20231220-052045.png'>
	
#. Goto k8s-setup pod.
	.. raw:: html
	
		<img src='/docs/_static/media/image-20231220-061903.png'>
	
#. Open terminal of k8s-setup.
	.. raw:: html
	
		<img src='/docs/_static/media/image-20231220-062133.png'>
	
#. List down files under the k8sfiles directory and edit the deployment file under the respective module directory.
	.. raw:: html
	
		<img src='/docs/_static/media/image-20231220-071940.png'>
	
#. Now, Find the size parameter, change the volume value for the respective module, and save the file.
	.. raw:: html
	
		<img src='/docs/_static/media/image-20231220-080926.png'>
	
	.. raw:: html
	
		<p><strong>Note: </strong>To change the value of Kafka broker and Kafka zookeeper. Follow the same above steps but the difference will be <code>deploy_kafka.yaml</code> will hold value for both the modules for broker and zookeeper.<br/><br/>For <code>Broker</code>:<br/></p>
	
	.. raw:: html
	
		<img src='/docs/_static/media/image-20231220-085517.png'>
	
	.. raw:: html
	
		<p>For <code>Zookeeper</code>:<br/></p>
	
	.. raw:: html
	
		<img src='/docs/_static/media/image-20231220-090018.png'>
	
#. Now run the helm upgrade command for OpenSearch and Kafka.For OpenSearch run the following command:
	.. code-block:: text
	
		helm upgrade os opensearch/opensearch -f k8sfiles/os/deploy_os.yaml -n 4px
	
	
	.. raw:: html
	
		<p><br/>For Kafka <code>zookeeper</code> and <code>broker</code>run the following command:</p>
	
	.. code-block:: text
	
		helm upgrade kafka bitnami/kafka -f k8sfiles/kafka/deploy_kafka.yaml -n 4px
	
	



.. toctree::
