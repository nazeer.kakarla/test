#############################################
INSTALLATION OF MULTI-NODE KUBERNETES CLUSTER
#############################################


*************************************
Install Multi-node Kubernetes Cluster
*************************************

.. raw:: html

	<p>This page will walk you through the installation process of the Kubernetes cluster with 1 Master Node and 3 Worker Nodes on RHEL/Rocky/Centos Linux Distro.</p>

***********************************
Installation Steps for Master Nodes
***********************************


Step 1: Install Docker
----------------------

* Configure the yum repo to install the docker
	.. code-block:: bash
	
		sudo yum install -y yum-utils
		sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
	
	
* Install and configure the docker
	.. code-block:: bash
	
		sudo yum install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
		sudo usermod -aG docker $USER
		newgrp docker
	
	
* Start and enable the docker service
	.. code-block:: bash
	
		sudo systemctl start docker
		sudo systemctl enable docker.service
		sudo systemctl enable containerd.service
	
	




Step 2: Install Kubernetes
--------------------------

* Configure the yum repo to install Kubernetes
	.. code-block:: bash
	
		cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
		[kubernetes]
		name=Kubernetes
		baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-\$basearch
		enabled=1
		gpgcheck=1
		gpgkey=https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
		exclude=kubelet kubeadm kubectl
		EOF
	
	
* Disable SELinux
	.. code-block:: bash
	
		sudo setenforce 0
		sudo sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config
	
	
* Disable swap space
	.. code-block:: bash
	
		sudo sed -i '/swap/d' /etc/fstab
		sudo swapoff -a
	
	
* Configure Firewall rules
	.. code-block:: bash
	
		sudo firewall-cmd --permanent --add-port=6443/tcp
		sudo firewall-cmd --permanent --add-port=2379-2380/tcp
		sudo firewall-cmd --permanent --add-port=10250/tcp
		sudo firewall-cmd --permanent --add-port=10251/tcp
		sudo firewall-cmd --permanent --add-port=10252/tcp
		sudo firewall-cmd --permanent --add-port=10255/tcp
		sudo firewall-cmd --reload
	
	
* Install necessary Kubernetes tools (kubelet, kubectl, kubeadm)
	.. code-block:: bash
	
		sudo yum install -y kubelet kubeadm kubectl --disableexcludes=kubernetes
		sudo systemctl enable --now kubelet
	
	
* Initialize Master Node
	.. code-block:: bash
	
		sudo kubeadm init --pod-network-cidr=10.244.0.0/16
	
	
	* copy the kubeadm join tokenThis will be used in the last step of the worker node where it will be used to join with k8s master node.Ex: kubeadm join < clustet IP>:6443 --token < > \ --discovery-token-ca-cert-hash < sha256:\*\*\*\*\*\*\*\* >
	
	
	
	.. code-block:: bash
	
		sudo mkdir -p $HOME/.kube
		sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
		sudo chown $(id -u):$(id -g) $HOME/.kube/config
	
	
* Install flannel networking driver
	.. code-block:: bash
	
		kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
	
	
* Configure networking requirements
	.. code-block:: text
	
		cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
		overlay
		br_netfilter
		EOF
	
	
	.. code-block:: text
	
		cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
		net.bridge.bridge-nf-call-iptables  = 1
		net.bridge.bridge-nf-call-ip6tables = 1
		net.ipv4.ip_forward                 = 1
		EOF
	
	
	.. code-block:: text
	
		sudo sysctl --system
	
	
	.. code-block:: text
	
		sysctl net.bridge.bridge-nf-call-iptables net.bridge.bridge-nf-call-ip6tables net.ipv4.ip_forward
	
	
* If pods are not stable apply the below configuration
	.. code-block:: text
	
		sudo mkdir -p /etc/containerd/
	
	
	.. code-block:: text
	
		containerd config default | sudo tee /etc/containerd/config.toml
	
	
	.. code-block:: text
	
		sudo sed -i 's/SystemdCgroup \= false/SystemdCgroup \= true/g' /etc/containerd/config.toml
	
	



***********************************
Installation Steps for Worker Nodes
***********************************

.. raw:: html

	<p>Now, Perform the below steps on all the worker nodes one by one.</p>


Step 1: Install Docker
----------------------

* Configure the yum repo to install the docker
	.. code-block:: bash
	
		sudo yum install -y yum-utils
		sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
	
	
* Install and configure the docker
	.. code-block:: bash
	
		sudo yum install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
		sudo usermod -aG docker $USER
		newgrp docker
	
	
* Start and enable the docker service
	.. code-block:: bash
	
		sudo systemctl start docker
		sudo systemctl enable docker.service
		sudo systemctl enable containerd.service
	
	




Step 2: Install Kubernetes
--------------------------

* Configure the yum repo to install Kubernetes
	.. code-block:: bash
	
		cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
		[kubernetes]
		name=Kubernetes
		baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-\$basearch
		enabled=1
		gpgcheck=1
		gpgkey=https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
		exclude=kubelet kubeadm kubectl
		EOF
	
	
* Disable SELinux
	.. code-block:: bash
	
		sudo setenforce 0
		sudo sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config
	
	
* Disable swap space
	.. code-block:: bash
	
		sudo sed -i '/swap/d' /etc/fstab
		sudo swapoff -a
	
	
* Configure Firewall rules
	.. code-block:: bash
	
		sudo firewall-cmd --permanent --add-port=6443/tcp
		sudo firewall-cmd --permanent --add-port=2379-2380/tcp
		sudo firewall-cmd --permanent --add-port=10250/tcp
		sudo firewall-cmd --permanent --add-port=10251/tcp
		sudo firewall-cmd --permanent --add-port=10252/tcp
		sudo firewall-cmd --permanent --add-port=10255/tcp
		sudo firewall-cmd --reload
	
	
* Install necessary Kubernetes tools (kubelet, kubectl, kubeadm)
	.. code-block:: bash
	
		sudo yum install -y kubelet kubeadm kubectl --disableexcludes=kubernetes
		sudo systemctl enable --now kubelet
	
	
* Configure networking requirements
	.. code-block:: text
	
		cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
		overlay
		br_netfilter
		EOF
	
	
	.. code-block:: text
	
		cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
		net.bridge.bridge-nf-call-iptables  = 1
		net.bridge.bridge-nf-call-ip6tables = 1
		net.ipv4.ip_forward                 = 1
		EOF
	
	
	.. code-block:: text
	
		sudo sysctl --system
	
	
	.. code-block:: text
	
		sysctl net.bridge.bridge-nf-call-iptables net.bridge.bridge-nf-call-ip6tables net.ipv4.ip_forward
	
	
* Run the join command which you copied after the initialization of the master node.
	.. code-block:: text
	
		kubeadm join < clustet IP>:6443 --token < > \ --discovery-token-ca-cert-hash < sha256:******** >
	
	
* Now, Login to the master node run the get nodes command, and check if all nodes are in ready state or not.
	.. code-block:: text
	
		kubectl get nodes
	
	
	.. code-block:: text
	
		[demo@rnd-1 4px]$ kubectl get nodes
		NAME    STATUS   ROLES           AGE   VERSION
		rnd-1   Ready    control-plane   35d   v1.28.2
		rnd-2   Ready    <none>          35d   v1.28.2
		rnd-3   Ready    <none>          35d   v1.28.2
		rnd-4   Ready    <none>          35d   v1.28.2
	
	




References:https://phoenixnap.com/kb/how-to-install-kubernetes-on-centoshttps://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/ https://kubernetes.io/docs/tasks/tools/install-kubectl/ 
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

.. toctree::
