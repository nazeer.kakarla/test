#######################
Helm Installation Guide
#######################


.. raw:: html

	<p>This document will guide you to install helm so that you can install storage-class, Kafka, OpenSearch &amp; OpenSearch-Dashboard pods in your cluster. Helm should be installed in the k8s master VM.</p>

.. raw:: html

	<p><strong>Prerequisite:</strong></p>

* curl installed (If curl is not installed, Install it using package manager such as yum, apt-get)
* permissions to make a file executable



.. raw:: html

	<p><strong>Steps: (Install Helm in RHEL OS)</strong></p>

#. Open Terminal of K8S Master VM and run the following commands:
	#. Downloads the Helm Script using curl
		.. code-block:: text
		
			curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
		
		
	#. Make the script executable
		.. code-block:: text
		
			chmod 700 get_helm.sh
		
		
	#. Execute the script.
		.. code-block:: text
		
			./get_helm.sh
		
		
	
	
	
#. Helm is now configured in your system and can be used to install any package using helm chart. You can check the Helm version and manual page using the following commands.
	.. code-block:: text
	
		helm version
		helm --help
	
	



.. raw:: html

	<p style="margin-left: 30.0px;">And check the manual page using</p>

#. Steps to Install a Helm Chart of any Application:
	.. code-block:: text
	
		helm repo add repo-name repository-url
		helm repo update
	
	
#. Install the repository you added
	.. code-block:: text
	
		helm install <RELEASE_NAME> <CHART_NAME>
	
	
#. You can then check all the available helm charts and their status using
	.. code-block:: text
	
		helm list
	
	



.. raw:: html

	<p><strong>Reference Link:</strong> <a href="https://helm.sh/docs/intro/install/"> Helm | Installing Helm</a> </p>

.. toctree::
