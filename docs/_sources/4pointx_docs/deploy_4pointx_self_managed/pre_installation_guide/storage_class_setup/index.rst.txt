###################
Storage Class Setup
###################


.. raw:: html

	<p>Once you have setup the nfs server on your VM, next you need to create a storage class out of the NFS which will later be specified while allocating PV and PVC. You can do this by deploying Storage Class NFS External Provisioner using helm on Master Node VM.</p>

.. raw:: html

	<p><strong>Instructions:</strong></p>

.. raw:: html

	<p>Follow the given steps:</p>

#. Add the NFS External Provisioner Helm repository.
	.. code-block:: text
	
		helm repo add nfs-subdir-external-provisioner https://kubernetes-sigs.github.io/nfs-subdir-external-provisioner/
	
	
#. Install the NFS External Provisioner Helm chart:
	.. code-block:: text
	
		helm install nfs-subdir-external-provisioner nfs-subdir-external-provisioner/nfs-subdir-external-provisioner \
		--set nfs.server=35.XX.XX.135 \
		--set nfs.path=/nfs \
		--set storageClass.name=standard \
		--set storageClass.allowVolumeExpansion=true
	
	
#. Now your storage class is up and ready to be used. (Make sure to remember the storageClass.name you choose in the previous step, you can choose any name but you need to remember this as this will be later used while deploying pods in the cluster)
	.. code-block:: text
	
		kubectl get storageclass -n <namespce>
	
	
	.. raw:: html
	
		<p>Use the above command to get information about your storage class created.</p>
	



.. toctree::
