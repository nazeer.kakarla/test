############################
SETUP & ACCESS K8S DASHBOARD
############################


.. raw:: html

	<p>This is a guide regarding setting up k8s dashboard and using it to manage and monitor the health of your entire cluster. To achieve this configuration, three things are required:</p>

* Metrics Server (fetches data in k8s cluster and can be used as)
	.. code-block:: text
	
		kubectl top nodes 
		kubectl top pods -n 4px
	
	
* Metrics-Data-scraper (sends data from metrics server to k8s dashboard)
* K8s dashboard (used to view all the metrics and monitor the pods and nodes created in the cluster) (https://<MASTER-IP>:30087/) (The IP address used is the IP address of master node)



*************
 Instructions
*************

#. Install k8s dashboard in the namespace kubernetes-dashboard (this installs data scraper too)
	.. code-block:: text
	
		kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.2.0/aio/deploy/recommended.yaml
	
	
#. Patch the dashboard to allow skipping login, change svc type to NodePort and specify the open port 30031 to expose the service on.
	.. code-block:: text
	
		kubectl patch deployment kubernetes-dashboard -n kubernetes-dashboard --type 'json' -p '[{"op": "add", "path": "/spec/template/spec/containers/0/args/-", "value": "--enable-skip-login"}]'
		kubectl patch svc kubernetes-dashboard -n kubernetes-dashboard --type='json' -p '[{"op":"replace","path":"/spec/type","value":"NodePort"},{"op":"replace","path":"/spec/ports/0/nodePort","value":30031}]'
	
	
#. Install metrics server (DON’T specify a namespace as this automatically gets installed in kube-system namespace)
	.. code-block:: text
	
		kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.4.2/components.yaml
	
	
#. Patch the metrics server to work with insecure TLS.
	.. code-block:: text
	
		kubectl patch deployment metrics-server -n kube-system --type 'json' -p '[{"op": "add", "path": "/spec/template/spec/containers/0/args/-", "value": "--kubelet-insecure-tls"}]'
	
	
	.. raw:: html
	
		<p>If by any chance kubectl top command doesn’t work, edit using the following command</p>
	
	.. code-block:: text
	
		kubectl -n kube-system edit deploy metrics-server
	
	
	.. raw:: html
	
		<p>And add the following command block under containers block as shown below:</p>
	
	.. code-block:: text
	
		containers:
		      - args:
		        - --cert-dir=/tmp
		        - --secure-port=4443
		        - --kubelet-preferred-address-types=InternalIP,ExternalIP,Hostname
		        - --kubelet-use-node-status-port
		        - --metric-resolution=15s
		        command:
		        - /metrics-server
		        - --kubelet-insecure-tls
		        - --kubelet-preferred-address-types=InternalIP
	
	
#. Next we need a token to login in our dashboard admin-user account
	.. code-block:: text
	
		 kubectl -n kubernetes-dashboard create token k8s-dashboard-kubernetes-dashboard --duration=488h
	
	
#. Navigate to the IP address https://<MASTER-IP>>:30087/ ,accept the warning and continue. Enter the token generated the the previous step.



.. toctree::
