###############
Security - RBAC
###############




*****
Roles
*****

************
Plant Viewer
************

* Can view everything in the Plant. Has read-only access to all modules.
	* Configuration (Pipelines, Assets, Monitors, Devices etc.)
	* CM, EMS, PA
	
	
	
* Read-only access to Plant tenant. Cannot create visualisations, dashboards and index patterns









*************
Plant Analyst
*************

* Can view everything in the Plant. Has read-only access to all modules.
	* Configuration (Pipelines, Assets, Monitors, Devices etc.)
	* CM, EMS, PA
	
	
	
* Read-write access to Plant tenant. Can create visualisations, dashboards and index patterns









***********
Plant Admin
***********

.. raw:: html

	<p>E.g., HSM2 Admin, CRM2 Admin</p>

* Can view and edit everything in the Plant. Has read-write access to all modules.
	* Configuration
		* Create and edit Functions, Assets and Monitors
		* Create and edit Pipelines
		* Register and unregister Devices (Deprecated. This is now done in create/edit Pipelines)
		
		
		
	* CM, EMS, PA
	
	
	
* Read-write access to Plant tenant. Can create visualisations, dashboards and index patterns









**********
Site Admin
**********

.. raw:: html

	<p>E.g., Demo Admin</p>

* Can view and edit everything in the Site. Has read-write access to all modules.
	* Configuration
		* Create and edit Functions, Assets and Monitors
		* Create and edit Pipelines
		* Register and unregister Devices (Deprecated. This is now done in create/edit Pipelines)
		
		
		
	* CM, EMS, PA
	
	
	
* Read-write access to all Plant tenants. Can create visualisations, dashboards and index patterns









.. toctree::
