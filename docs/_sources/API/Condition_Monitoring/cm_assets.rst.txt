**CM Assets**
=============


**CM Assets List**
+++++++++++++++++++++

.. http:get:: /_cm/assets/_list

  **Description** Fetch the list of Assets which is used in CM

  **Http Method** GET

  **ENDPOINT** ``/_cm/assets/_list``

  **Request Header** ``Authorization`` `Basic <token>`
    
  **Request Parameters**

  URL Parameters

    - ``function_id`` (string, *required*): The ID of the associated function.
    - ``from`` (string, *required*): The start time of the time range.
    - ``to`` (string, *required*): The end time of the time range


  **Sample Request**

    .. code-block:: sh
    
        curl --location 'https://app.4pointx.com/_cm/assets/_list?to=2023-09-17T19%3A10%3A00Z&function_id=45c98f40-75e3-49e7-b178-d25ec878db79&from=2021-05-17T19%3A10%3A00Z' \
        --header 'Authorization: Basic <token>'


  **Sample Response**

  Success

    .. sourcecode:: json

      [
        {
            "asset_name": "Demo Asset",
            "asset_status": "warning",
            "asset_faults": [],
            "event_start": 1712737861,
            "event_severity": 1,
            "event_assignee": "test@tesss.com",
            "event_id": "bc39d80f-00bb-43b3-b287-211cbfdf2f4c",
            "event_status": "Under Investigation",
            "event_label": "TestEvent01",
            "asset_id": "d3fe9877-b4f4-477e-a790-96a22a4cc665"
        },
        {
            "asset_name": "test-asset-dnd",
            "asset_status": "",
            "asset_faults": [],
            "event_start": "",
            "event_severity": "",
            "event_assignee": "",
            "event_id": "",
            "event_status": "",
            "event_label": "",
            "asset_id": "b5f82547-070c-460b-a9ac-c99aa6f73e79"
        },
        {
            "asset_name": "Demo Asset",
            "asset_status": "",
            "asset_faults": [],
            "event_start": "",
            "event_severity": "",
            "event_assignee": "",
            "event_id": "",
            "event_status": "",
            "event_label": "",
            "asset_id": "547618d4-c481-48e5-b2a0-ae810a94883a"
        },
        {
            "asset_name": "RM2 Motor",
            "asset_status": "",
            "asset_faults": [],
            "event_start": "",
            "event_severity": "",
            "event_assignee": "",
            "event_id": "",
            "event_status": "",
            "event_label": "",
            "asset_id": "b29bee18-e42f-436b-9464-77bab60388d6"
        },
      ]

  Error

    .. sourcecode:: json

      {
        "error": {
          "status": 400,
          "message": "Excess Inputs"
        }
      }


    .. sourcecode:: json

      {
        "error": {
          "status": 400,
          "message": "Invalid Function Id"
        }
      }
