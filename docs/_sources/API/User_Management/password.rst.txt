**Password**
========================

**Generate OTP**
+++++++++++++++++++++++++++
.. http:post:: /_config/users/password/generate_otp
  
  **Description** Generates OTP and emails it to the user to initiate the password reset process.

  **HTTP Method** POST
  
  **Endpoint:** ``/_config/users/password/generate_otp``

  **Request Parameters** 

  Body Parameters
    
      - ``email`` (string, *required*): The unique email ID of the user.

  **Sample Request**

      .. code-block:: sh

         curl -X POST \
               'https://app.4pointx.com/_config/users/password/generate_otp' \
               -H 'Content-Type: application/json' \
               -d '{
                  "email": "support@4point.com"
               }'
        

  **Sample Response**

  Success

   .. sourcecode:: json

      
         "An OTP has been sent to your email address"
      

  Error 

 
   .. sourcecode:: json

      {
         "error": {
            "status": 400,
            "message": "Insufficient inputs"
         }
      }
   
   .. sourcecode:: json

      {
         "error": {
            "status": 409,
            "message": "User Doesn't Exists"
         }
      }
        

   .. sourcecode:: json

      {
         "error": {
            "status": 500,
            "message": "Something went wrong."
         }
      }



**Validate OTP**
+++++++++++++++++++++++++++
.. http:post:: /_config/users/password/validate_otp

  **Description** Validates if the provided OTP matches the stored OTP and is not expired during the password reset process.

  **HTTP Method** POST

  **Endpoint** ``/_config/users/password/validate_otp``

  **Request Parameters**

  **Sample Request**

    .. code-block:: sh

      curl -X POST \
               'https://app.4pointx.com/_config/users/validate_otp' \
               -H 'Content-Type: application/json' \
               -d '{
                  "email": "support@4point.com",
                  "otp": "123456"
               }'

  **Sample Response**

  Success 

    .. sourcecode:: json

        "OTP verified successfully"


  Error 

    .. sourcecode:: json

      {
         "error": {
            "status": 400,
            "message": "Invalid request body"
         }
      }
   
    .. sourcecode:: json

      {
         "error": {
            "status": 401,
            "message": ""Invalid OTP"
         }
      }

    .. sourcecode:: json

      {
         "error": {
            "status": 404,
            "message": "Invalid OTP"
         }
      }


    .. sourcecode:: json

      {
         "error": {
            "status": 409,
            "message": "OTP has expired, please request a new one"
         }
      }
   
    .. sourcecode:: json

      {
         "error": {
            "status": 409,
            "message": "User Doesn't Exists"
         }
      }




**Update User Password**
+++++++++++++++++++++++++++++++++
.. http:put:: /_config/users/password/
  
    **Description** Updates the user’s password after verifying their identity using the OTP.

    **HTTP Method** PUT
    
    **Endpoint** ``/_config/users/password/``


    **Request Parameters**

    Body Parameters

      - ``email`` (string, *required*):  The unique email of the user.
      - ``otp`` (string, *required*):  The one-time password.
      - ``new_password`` (string, *required*): The password that the user wants to set as their new password.

    **Sample Request**

      .. code-block:: sh
         
         curl -X PUT \
            'https://app.4pointx.com/_config/users/password/' \
            -H 'Content-Type: application/json' \
            -d '{
                "email": "support@4pointx.com",
                "otp": "123456",
                "new_password": "12345678"                
            }'
            

    **Sample Response**

    Success

      .. sourcecode:: json

         "Password updated successfully"
        

    Error

      .. sourcecode:: json

         {
            "error": {
               "status": 400,
               "message": "Invalid request body"
            }
         }


      .. sourcecode:: json

         {
            "error": {
               "status": 404,
               "message": "Please request a new one OTP"
            }
         }

      .. sourcecode:: json
         
         {
            "error": {
               "status": 409,
               "message": "OTP has expired, please request a new one"
            }
         }
            
      .. sourcecode:: json

         {
            "error": {
               "status": 409,
               "message": "User Doesn't Exists"
            }
         }
      .. sourcecode:: json

         {
            "error": {
               "status": 409,
               "message": "OTP has expired, please request a new one"
            }
         }


      .. sourcecode:: json

         {
            "error": {
               "status": 500,
               "message": "Failed to update password"
            }
         }





