**Licenses**
======================


**Generate a License**
+++++++++++++++++++++++

.. http:post:: /_config/license
  
  **Description** This is used for generating new license.

  **HTTP Method** POST
  
  **Endpoint:** ``/_config/license``

  **Request Header** ``Authorization`` `Basic <token>`


  **Request Parameters** 

  Body Parameters



    - ``license_name`` (string, *required*): The name of the license to be created.
    - ``plant_id`` (string, *required*): The ID of the plant against which the license is to be generated.
    - ``site_id`` (string, *required*): The ID of the site against  which the license is to be generated. 
    - ``license_validity`` (string, *required*): licen se validity in days/months or year.
    - ``app`` (string, *required*): The name of the application against which the license is to be generated.
    - ``license_quota`` (string, *optional*): The quota of the monitors to be managed.
    - ``license_description`` (string, *optional*): The description of the license like who issued it or who will be using it and so on.

  **Sample Request**

    .. code-block:: sh
      
      
      curl --location 'https://app.4pointx.com/_config/license' \
        --header 'Content-Type: application/json' \
        --header 'Authorization: Basic <token>' \
        --data '{
            "license_name":"L1",
            "plant_id": "7a45d8sde5-92d2-46a7-92d3-85aa4bd57bba",
            "site_id": "4230adcbawd-f349-4780-bf79-4a8c94a3b13b",
            "license_validity": "1y",
            "app":"EMS",
            "license_quota": "100",
            "license_description": "Description"
            }'
  
  
  **Sample Response**

  Success 

    .. sourcecode:: json

      {
        "id": "96978782-b0ea-49dc-9f87-2edf8ba2f126"
      }
          

  Error 

    .. sourcecode:: json

      {
        "error": {
          "status": 400,
          "message": "Invalid Content Type"
        }
      }
          
          

    .. sourcecode:: json

      {
        "error": {
          "status": 400,
          "message": "Insufficient Inputs"
        }
      }

          
          
    .. sourcecode:: json

      {
        "error": {
          "status": 403,
          "message": "Not authorised"
        }
      }




**Update License**
++++++++++++++++++++++++

.. http:put:: /_config/license/<license_id>
  
    **Description** This endpoint allows you to update the details of a specific license by providing its ID.

    **HTTP Method** PUT
    
    **Endpoint** ``/_config/license/<license_id>``

    **Request Header** ``Authorization`` `Basic <token>`

    **Request Parameters**

    Body Parameters
    
      - ``license_name`` (string, *optional*): The updated license name.
      - ``plant_id`` (string, *optional*): The updated name of the plant id.
      - ``site_id`` (string, *optional*): The updated name of the site id.
      - ``license_description`` (string, *optional*): The updated license description.
      - ``license_validity`` (string, *optional*): The updated license validity.
      - ``app`` (string, *optional*): The updated name of the application.
      - ``license_quota`` (string, *optional*): The updated license quota.
      - ``license_description`` (string, *optional*): The updated license description.
      - ``status`` (string, *optional*): The updated status.

    URL Parameters

      - ``license_id`` (string, *required*): The ID of the license to be updated.


    **Sample Request**

      .. code-block:: sh
      
          curl --location --request PUT 'https://app.4pointx.com/_config/license/96978782-b0ea-49dc-9f87-2edf8ba2f126' \
         --header 'Content-Type: application/json' \
         --header 'Authorization: Basic YWRtaW46clpvcGN1S3FVSXFteXJPVUNibDVTZGx4bVQzYU55ZG0=' \
         --data '
            {
            "license_name":"L1",
            "plant_id": "7a45d8sde5-92d2-46a7-92d3-85aa4bd57bba",
            "site_id": "4230adcbawd-f349-4780-bf79-4a8c94a3b13b",
            "license_validity": "1y",
            "app":"EMS",
            "license_quota": "100",
            "license_description": "Description"
            }'

    **Sample Response**

    Success

      .. sourcecode:: json
              
              
         "license Updated"
          
          
                                      

    Error

      .. sourcecode:: json

        {
          "error":{
            "status": 400,
            "message": "Invalid Content Type"
            }
        }

          

      .. sourcecode:: json


        {
          "error":{
            "status": 400,
            "message":  "Insufficient Inputs"
            }
        }


      .. sourcecode:: json

        {
         "error":{
            "status": 403,
            "message": "not authorised"
            }
        }


      .. sourcecode:: json

        {
         "error":{
            "status": 400,
            "message": "Invalid license"
            }
        }









**Get License**
+++++++++++++++++++++++

.. http:get:: /_config/license/<license_id>

    
    **Description** This endpoint allows you to retrieve details about a specific license by providing its license_id.



    **HTTP Method** GET


    **Endpoint** ``/_config/license/<license_idy>``

    **Request Header** ``Authorization`` `Basic <token>`

    **Request Parameters**
      
    URL Parameters

      - ``license_id`` (string, *required*): The license id of the license to be retrieved.
    

    **Sample Request**

      .. code-block:: sh

         curl --location 'https://app.4pointx.com/_config/license/96978782-b0ea-49dc-9f87-2edf8ba2f126' \
         --header 'Authorization: Basic <token>'

    **Sample Response**

    Success 

      .. code-block:: json
            
        {
            "license_id": "f3567a6a-8b57-4ae5-9584-b11dd8926c79",
            "license_key": ">+oV]G7IV%syFP49NizdKvx?yx19RYoP",
            "license_name": "t1",
            "plant_id": "7a45d8e5-92d2-46a7-92d3-85aa4bd57bba",
            "site_id": "4230adcb-f349-4780-bf79-4a8c94a3b13b",
            "created_at": 1709701645,
            "activated_at": 1709708778,
            "license_validity": "1y",
            "expiry_date": 1741244778,
            "app": "EMS",
            "license_quota": "200",
            "status": "Active",
            "license_description": "hello",
            "license_used": 30
        }

    Error 


      .. sourcecode:: json

        {
          "error": {
            "status": 405,
            "message": "Method not allowed"
          }
        }
        

      .. sourcecode:: json

        {
          "error": {
            "status": 400,
            "message": "Data not found"
          }
        }


      .. sourcecode:: json

        {
          "error": {
            "status": 403,
            "message": "Not Authorised"
          }
        }
          






**Fetch List of Licenses**
+++++++++++++++++++++++++++++
.. http:GET:: /_config/license/_list

    **Description** This endpoint allows you to retrieve a list of all licenses under given site id.

    **HTTP Method** GET

    **Endpoint** ``/_config/license/_list``

    **Request Header** ``Authorization`` `Basic <token>`

    **Request Parameters**

    Query Parameters


      - ``site_id`` (string, *required*): The ID of the site for which you want to list licenses.
     

    **Sample Request**

      .. code-block:: sh
        
         curl --location 'https://app.4pointx.com/_config/license/_list?site_id=e29ae75e-dd88-45ef-90cb-cefb3807af70' \
         --header 'Authorization: Basic <token>' \
         --data ''

    **Sample Response**

    Success 

      .. code-block:: json
          
        [
            {
                "license_id": "1",
                "license_key": ">+oVG7IV%syFP49NizdKvx?yx19RYoP",
                "license_name": "l1",
                "plant_id": "7a45d8e5-92d2-46a7-92d3-85aa4bd57bba",
                "site_id": "4230adcb-f349-4780-bf79-4a8c94a3b13b",
                "created_at": 1709701645,
                "activated_at": 1709708778,
                "license_validity": "1y",
                "expiry_date": 1741244778,
                "app": "EMS",
                "license_quota": "100",
                "status": "Active",
                "license_description": "hello",
                "license_used": 30
            },
            {
                "license_id": "2",
                "license_key": ">+oVG7IV%syFP49NizdKvx?yx19RYoP",
                "license_name": "l2",
                "plant_id": "7a45d8e5-92d2-46a7-92d3-85aa4bd57bba",
                "site_id": "4230adcb-f349-4780-bf79-4a8c94a3b13b",
                "created_at": 1709701645,
                "activated_at": 1709708778,
                "license_validity": "1y",
                "expiry_date": 1741244778,
                "app": "PDM",
                "license_quota": "200",
                "status": "Active",
                "license_description": "hello",
                "license_used": 10
            },
            {
                "license_id": "3",
                "license_key": ">+oVG7IV%syFP49NizdKvx?yx19RYoP",
                "license_name": "l3",
                "plant_id": "7a45d8e5-92d2-46a7-92d3-85aa4bd57bba",
                "site_id": "4230adcb-f349-4780-bf79-4a8c94a3b13b",
                "created_at": 1709701645,
                "activated_at": 1709708778,
                "license_validity": "1y",
                "expiry_date": 1741244778,
                "app": "CBM",
                "license_quota": "100",
                "status": "Active",
                "license_description": "hello",
                "license_used": 40
            }
        ]

    
    Error 

      .. sourcecode:: json

        {
          "error": {
            "status": 400,
            "message":  "Insufficient Inputs"
          }
        }

            

            
**Activate Licenses**
+++++++++++++++++++++++++++++
.. http:GET:: /_config/license/_activate

    **Description** This endpoint allows you to activate license.

    **HTTP Method** PUT

    **Endpoint** ``/_config/license/_activate``

    **Request Header** ``Authorization`` `Basic <token>`

    **Request Parameters**

    Body Parameters


      - ``license_key`` (string, *required*): The license key of the license to be activated.
     

    **Sample Request**

      .. code-block:: sh
        
         curl --location 'https://app.4pointx.com/_config/license/_activate' \
         --header 'Authorization: Basic <token>' \
         --data '{
            "license_key": "9[uY@]WT@zvoasdas?NweOZSBr2A8*ELr%t%"
         }'

    **Sample Response**

    Success

      .. sourcecode:: json
        {
            "license has been activated successfully"
        }



    Error

      .. sourcecode:: json

        {
          "error":{
            "status": 400,
            "message": "Invalid Content Type"
            }
        }

          

      .. sourcecode:: json


        {
          "error":{
            "status": 400,
            "message":  "Insufficient Inputs"
            }
        }


      .. sourcecode:: json

        {
         "error":{
            "status": 403,
            "message": "not authorised"
            }
        }


      .. sourcecode:: json

        {
         "error":{
            "status": 400,
            "message": "License does not exist"
            }
        }


      .. sourcecode:: json

        {
         "error":{
            "status": 400,
            "message": "License either in-use/expired/revoked"
            }
        }


      .. sourcecode:: json

        {
         "error":{
            "status": 500,
            "message": "license has been not been activated successfully"
            }
        }