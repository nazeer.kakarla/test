**Pipelines (Infinite Uptime)**
====================================


**Create IU Pipeline**
+++++++++++++++++++++++++++++++++++++
.. http:post:: /_config/pipelines/iu/
  
  **Description**  Create a new Infinite Uptime (IU) pipeline.

  **HTTP Method** POST
  
  **Endpoint:** ``/_config/pipelines/iu/``

  **Request Header** ``Authorization`` `Basic <token>`


  **Request Parameters** 

  Body Parameters



    - ``pipeline_name`` (string, *required*): The name of the pipeline.
    - ``data_type`` (string, *required*): The type of data the pipeline handles.
    - ``data_source`` (string, *required*): The source of the data.
    - ``data_source_subtype`` (string, *required*): The subtype of the data source.
    - ``settings`` (object, *required*):  Configuration settings for the pipeline.
          
          - ``mac_address`` (string, *required*): The MAC address of the device.
          - Additional parameters based on `data_source_subtype`:

               - If `data_source_subtype` is `OPC-UA`:

                     - ``server_url`` (string, *required*):  The URL of the OPC UA server.
                     - ``user_name`` (string, *required*):  The username for authentication.
                     - ``user_password`` (string, *required*):  The password for authentication.
               
               - If `data_source_subtype` is `File`:

                     - ``file_name`` (string, *required*):  The name of the file to be monitored.
                     - ``file_path`` (string, *required*):  The path to the file.
                     - ``time_field`` (string, *optional*): The field containing timestamp information.
                     - Additional parameters specific to the chosen `data_type`.

    - ``tags`` (array of objects, *required*): Tags associated with the pipeline.
         
          - ``tag_name`` (string, *required*): The name of the tag.
          - ``tag_desc`` (string, *required*): Description of the tag.
          - ``tag_unit`` (string, *required*): The unit of measurement for the tag.
          - Additional parameters specific to the chosen `data_type`
          
  


  **Sample Request**

    .. code-block:: sh
      
        curl --location 'https://app.4pointx.com/_config/pipelines/iu/' \
            --header 'Content-Type: application/json' \
            --header 'Authorization: Basic <token>' \
            --data '{
                "pipeline_name": "iu_pipeline-new",
                "site_id": "d2e66edd-f13f-4d00-a7b5-8cab9ef4488d",
                "plant_id": "e29ae75e-dd88-45ef-90cb-cefb3807af70",
                "function_id": "45c98f40-75e3-49e7-b178-d25ec878db79",
                "data_type": "Condition",
                "data_source": "Third-Party Apps",
                "data_source_subtype": "Infinite Uptime",
                "settings": {
                    "sampling_rate": 1
                },
                "tags": [
                    {
                        "tag_name": "velocity_x",
                        "address": "",
                        "tag_unit": "",
                        "tag_desc": ""
                    },
                    {
                        "tag_name": "velocity_y",
                        "address": "",
                        "tag_unit": "",
                        "tag_desc": ""
                    },
                    {
                        "tag_name": "velocity_z",
                        "address": "",
                        "tag_unit": "",
                        "tag_desc": ""
                    },
                    {
                        "tag_name": "acceleration_x",
                        "address": "",
                        "tag_unit": "",
                        "tag_desc": ""
                    },
                    {
                        "tag_name": "acceleration_y",
                        "address": "",
                        "tag_unit": "",
                        "tag_desc": ""
                    },
                    {
                        "tag_name": "acceleration_z",
                        "address": "",
                        "tag_unit": "",
                        "tag_desc": ""
                    },
                    {
                        "tag_name": "total_acceleration",
                        "address": "",
                        "tag_unit": "",
                        "tag_desc": ""
                    },
                    {
                        "tag_name": "audio",
                        "address": "",
                        "tag_unit": "",
                        "tag_desc": ""
                    },
                    {
                        "tag_name": "temperature",
                        "address": "",
                        "tag_unit": "",
                        "tag_desc": ""
                    }
                ],
                "registration": [],
                "function_enabled_in_registration": false
            }'
     
  **Sample Response**

  Success 

    .. sourcecode:: json

          {
            "id": "e894a4da-469d-4261-954b-5a9151c096ad"
          }
          

  Error 
    
    .. sourcecode:: json

      {
        "error": {
          "status": 400,
          "message": "Invalid Content Type"
        }
      }
          
          

    .. sourcecode:: json

      {
        "error": {
          "status": 400,
          "message": "Insufficient Inputs"
        }
      }
   
    .. sourcecode:: json

      {
        "error": {
          "status": 400,
          "message": "Invalid Plant id 2830-129037-717-2872"
        }
      }

          
          
  


**Delete Pipeline**
++++++++++++++++++++
.. http:delete:: /_config/pipelines/iu/<pipeline_id>

  **Description** Delete an IU pipeline.

  **HTTP Method** DELETE

  **Endpoint** ``/_config/pipelines/iu/<pipeline_id>``

  **Request Header** ``Authorization`` `Basic <token>`


  **Request Parameters**

  URL Parameters

    - ``pipeline_id`` (string, *required*) The ID of the pipeline to be deleted.

  
  **Sample Request**

    .. code-block:: sh
      
      curl --location --request DELETE 'https://app.4pointx.com/_config/pipelines/iu/e894a4da-469d-4261-954b-5a9151c096ad' \
           --header 'Authorization: Basic <token>' \
           --data ''

  **Sample Response**

  Success 

    .. sourcecode:: sh

        "Pipeline deleted"

  Error 

    .. sourcecode:: json

      {
        "error": {
          "status": 400,
          "message": "Invalid Pipeline 12839-129837-4837-1289"
        }
      }

        
      





**Update Pipeline**
+++++++++++++++++++++
.. http:put:: /_config/pipelines/iu/<pipeline_id>
  
    **Description** Update an existing iu pipeline.

    **HTTP Method** PUT
    
    **Endpoint** ``/_config/pipelines/iu/<pipeline_id>``

    **Request Header** ``Authorization`` `Basic <token>`

    **Request Parameters**

    Body Parameters
    
      - ``pipeline_name`` (string, *optional*): The updated name of the pipeline.
      - ``settings`` (object, *optional*):  Updated configuration settings for the pipeline.
            
            - ``mac_address`` (string, *required*): The MAC address of the device.
            - Additional parameters based on `data_source_subtype`:

               - If `data_source_subtype` is `OPC-UA`:

                     - ``server_url`` (string, *required*):  The URL of the OPC UA server.
                     - ``user_name`` (string, *required*):  The username for authentication.
                     - ``user_password`` (string, *required*):  The password for authentication.
               
               - If `data_source_subtype` is `File`:

                     - ``file_name`` (string, *required*):  The name of the file to be monitored.
                     - ``file_path`` (string, *required*):  The path to the file.
                     - ``time_field`` (string, *optional*): The field containing timestamp information.
                     - Additional parameters specific to the chosen `data_type`.

      - ``tags`` (array of objects, *required*): Tags associated with the pipeline.
         
            - ``tag_name`` (string, *required*): The name of the tag.
            - ``tag_desc`` (string, *required*): Description of the tag.
            - ``tag_unit`` (string, *required*): The unit of measurement for the tag.
            - Additional parameters specific to the chosen `data_type`
            



    **Sample Request**

      .. code-block:: sh
      
          curl --location --request PUT 'https://app.4pointx.com/_config/pipelines/iu/e894a4da-469d-4261-954b-5a9151c096ad' \
               --header 'Content-Type: application/json' \
               --header 'Authorization: Basic <token>' \
               --data '{"pipeline_name":"mod test gateway 4sep",
                        "site_id":"4230adcb-f349-4780-bf79-4a8c94a3b13b",
                        "plant_id":"7a45d8e5-92d2-46a7-92d3-85aa4bd57bba",
                        "function_id":"2136f450-1ab0-45ed-96fa-fec9c32da324",
                        "data_type":"Condition","data_source":"iu IoT Gateway",
                        "data_source_subtype":"Modbus RTU",
                        "settings":{"port":"/dev/ttymxc5","stopbit":"1","bytesize":"8","parity":"N","baudrate":"9600","mac_address":"asdaskj","polling_rate":1},
                        "tags":[{"tag_name":"velocity_x","address":"s","tag_unit":"w","tag_desc":""},
                                {"tag_name":"velocity_y","address":"","tag_unit":"","tag_desc":""},
                                {"tag_name":"velocity_z","address":"","tag_unit":"","tag_desc":""},
                                {"tag_name":"acceleration_x","address":"","tag_unit":"","tag_desc":""},
                                {"tag_name":"acceleration_y","address":"","tag_unit":"","tag_desc":""},
                                {"tag_name":"acceleration_z","address":"","tag_unit":"","tag_desc":""},
                                {"tag_name":"total_acceleration","address":"","tag_unit":"","tag_desc":""},
                                {"tag_name":"audio","address":"","tag_unit":"","tag_desc":""},
                                {"tag_name":"temperature","address":"","tag_unit":"","tag_desc":""}],
                                "registration":[{"slave_id":"14","function_id":"2136f450-1ab0-45ed-96fa-fec9c32da324","function_name":"test_latest","registered_by_asset":true,"asset_id":"129a22cb-7006-4260-a2d7-0d17d78e41bf","asset_name":"trial2",
                                "asset_options":[{"value":"129a22cb-7006-4260-a2d7-0d17d78e41bf","inputDisplay":"trial2"},{"value":"42039075-bc51-4153-b61a-8dfd9b22e4ee","inputDisplay":"trial3"},{"value":"eefbbde6-0ede-4117-b266-4af8640a4eff","inputDisplay":"trial3"},{"value":"ac1c9638-0633-445a-bb3b-5d292508528c","inputDisplay":"test_4"}],"monitor_id":"977db698-de2e-4094-b812-9767ee8089ca","monitor_name":"new_mon_2","monitor_options":[{"value":"977db698-de2e-4094-b812-9767ee8089ca","inputDisplay":"new_mon_2"}]}],
                                "function_enabled_in_registration":false}'


    **Sample Response**

    Success

      .. sourcecode:: json
              
              
         "Pipeline Updated"
          
          
                                      

    Error

      .. sourcecode:: json

        {
          "error": {
            "status": 400,
            "message": "Invalid Content Type"
          }
        }

          

      .. sourcecode:: json


        {
          "error": {
            "status": 400,
            "message":  "Insufficient Inputs"
          }
        }


      .. sourcecode:: json

        {
          "error": {
            "status": 400,
            "message":  "Mac address already taken pipeline_id 128937-2189379-12839-1928"
          }
        }
      
      .. sourcecode:: json

        {
          "error": {
            "status": 400,
            "message":  "Monitor already registered in another pipeline"
          }
        }

      .. sourcecode:: json

        {
          "error": {
            "status": 400,
            "message":  "Invalid Input: <message>"
          }
        }


          






**Fetch Pipeline**
+++++++++++++++++++++
.. http:get:: /_config/pipelines/iu/<pipeline_id>

    
    **Description** Retrieve details of a specific iu pipeline.


    **HTTP Method** GET


    **Endpoint** ``/_config/pipelines/iu/<pipeline_id>``

    **Request Header** ``Authorization`` `Basic <token>`

    **Request Parameters**
      
    Query Parameters

      - ``mac_ids`` (array of strings, *optional*): An array of MAC addresses to identify pipelines.
        
    **Sample Request**

      .. code-block:: sh

          curl --location 'https://app.4pointx.com/_config/pipelines/iu/e894a4da-469d-4261-954b-5a9151c096ad' \
               --header 'Authorization: Basic <token>'
    
    **Sample Response**

    Success 

      .. code-block:: json
            
         {
            "function_id": "2136f450-1ab0-45ed-96fa-fec9c32da324",
            "plant_id": "7a45d8e5-92d2-46a7-92d3-85aa4bd57bba",
            "pipeline_id": "e894a4da-469d-4261-954b-5a9151c096ad",
            "created_at": 1697537125918,
            "data_type": "Condition",
            "data_source": "iu IoT Gateway",
            "pipeline_name": "test gateway 4sep",
            "status": "stopped",
            "total_event_count": 0,
            "data_source_subtype": "Modbus RTU",
            "firmware_update": 0,
            "data_directory": "/home/debian/data",
            "site_id": "4230adcb-f349-4780-bf79-4a8c94a3b13b",
            "settings": {
               "port": "/dev/ttymxc5",
               "stopbit": "1",
               "bytesize": "8",
               "parity": "N",
               "baudrate": "9600",
               "mac_address": "asdaskj",
               "sampling_rate": 1,
               "ip": ""
            },
            "tags": [
               {
                     "address": "s",
                     "tag_name": "velocity_x",
                     "tag_desc": "",
                     "tag_unit": "w"
               },
               {
                     "address": "",
                     "tag_name": "velocity_y",
                     "tag_desc": "",
                     "tag_unit": ""
               },
               {
                     "address": "",
                     "tag_name": "velocity_z",
                     "tag_desc": "",
                     "tag_unit": ""
               },
               {
                     "address": "",
                     "tag_name": "acceleration_x",
                     "tag_desc": "",
                     "tag_unit": ""
               },
               {
                     "address": "",
                     "tag_name": "acceleration_y",
                     "tag_desc": "",
                     "tag_unit": ""
               },
               {
                     "address": "",
                     "tag_name": "acceleration_z",
                     "tag_desc": "",
                     "tag_unit": ""
               },
               {
                     "address": "",
                     "tag_name": "total_acceleration",
                     "tag_desc": "",
                     "tag_unit": ""
               },
               {
                     "address": "",
                     "tag_name": "audio",
                     "tag_desc": "",
                     "tag_unit": ""
               },
               {
                     "address": "",
                     "tag_name": "temperature",
                     "tag_desc": "",
                     "tag_unit": ""
               }
            ]
         }

    Error 

      .. sourcecode:: json

        {
          "error": {
            "status": 400,
            "message":  "Invalid Pipeline 128937-32948-217360-3476"
          }
        }

          





