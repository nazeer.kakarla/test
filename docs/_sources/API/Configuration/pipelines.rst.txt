**Pipelines**
================


**Fetch List of Pipelines**
++++++++++++++++++++++++++++
.. http:get:: /_config/pipelines/_list

    
    **Description** Retrieve details of pipelines for a specific plant or function.

    **HTTP Method** GET

    **Endpoint** ``/_config/pipelines/_list``

    **Request Header** ``Authorization`` `Basic <token>`

    **Request Parameters**
      
    Query Parameters

      - ``function_id`` (string, *optional*):  The unique identifier of the function for which pipeline details are requested.
      - ``plant_id`` (string, *optional*):  The unique identifier of the plant for which pipeline details are requested.
     
    

    **Sample Request**

      .. code-block:: sh

        curl --location 'https://app.4pointx.com/_config/pipelines/_list?function_id=3626c23b-2070-48bb-baaa-1170a39c0f88' \
             --header 'Authorization: Basic <token>'


    **Sample Response**

    Success 

      .. code-block:: json

        [
           {
               "data_source_subtype": "File",
               "pipeline_id": "131911e7-4b97-4951-b16c-1cbc3046ca63",
               "pipeline_name": "energy-pipeline",
               "plant_name": "CRM-2",
               "function_name": "PLTCM",
               "data_type": "Energy Data",
               "data_source": "Edge IoT Gateway",
               "created_at": 1650994919467,
               "status": "stopping",
               "total_events": 0,
               "latest_event": [
                     1683519343000
               ],
               "heart_beats": { 
                 "log_parser": null, 
                 "stasher": 1695640695935, 
                 "producer": null, 
                 "gateway": 1695640695935 
               },
               "last_actions": [
                     {
                        "action": null,
                        "status": null,
                        "issued_at": null,
                        "completed_at": null
                     }
               ]
            },
            {
               "data_source_subtype": "File",
               "pipeline_id": "3cb67390-376e-4cc0-ba66-e780959a4529",
               "pipeline_name": "Opc-pipeline",
               "plant_name": "CRM-2",
               "function_name": "PLTCM",
               "data_type": "Process Data",
               "data_source": "Edge IoT Gateway",
               "created_at": 1649919341371,
               "status": "starting",
               "total_events": 0,
               "latest_event": [
                     1688469915000
               ],
               "heart_beats": { 
                 "log_parser": null, 
                 "stasher": 1695640695935, 
                 "producer": null, 
                 "gateway": 1695640695935 
               },
               "last_actions": [
                     {
                        "action": null,
                        "status": null,
                        "issued_at": null,
                        "completed_at": null
                     }
               ]
            },
         ]
            
          

    Error 

      .. sourcecode:: json

          {
            "error": {
              "status": 400,
              "message":  "Insufficient Inputs"
            }
          }

          


        
**Count Pipelines**
+++++++++++++++++++++++++++++
.. http:GET:: /_config/pipelines/_count

    **Description** Retrieve the total count of pipelines for a specific function or plant.

    **HTTP Method** GET

    **Endpoint** ``/_config/pipelines/_count``

    **Request Header** ``Authorization`` `Basic <token>`

    **Request Parameters**

    Query Parameters

      - ``plant_id`` (string, *optional*): The ID of the plant for which pipeline count is requested.
      - ``function_id`` (string, *optional*): The ID of the function for which pipeline count is requested.
      - ``status`` (string, *optional*): Filter pipelines by status (e.g., start, stop).

      
      
    **Sample Request**

      .. code-block:: sh
        
          curl --location 'https://app.4pointx.com/_config/pipelines/_count?function_id=45c98f40-75e3-49e7-b178-d25ec878db79' \
               --header 'Authorization: Basic <token>'

    **Sample Response**

    Success 

      .. code-block:: json
          
        8

    
    Error 

      .. sourcecode:: json

        {
          "error": {
            "status": 400,
            "message":  "Insufficient Inputs"
          }
        }


          

      .. sourcecode:: json

        {
          "error": {
            "status": 400,
            "message": "Excess Inputs"
          }
        }
      
            


**Execute Pipeline Action**
+++++++++++++++++++++++++++++
.. http:GET:: /_config/pipelines/_action

    **Description** Execute an action (e.g., start or stop) on one or more pipelines.

    **HTTP Method** POST

    **Endpoint** ``/_config/pipelines/_action``

    **Request Header** ``Authorization`` `Basic <token>`

    **Request Parameters**

    Body Parameters

      - ``pipeline_id`` (array of strings, *optional*): A list of pipeline IDs on which the action should be executed.
      - ``action`` (string, *optional*): The action to perform on the pipelines (e.g., "start" or "stop").

      
      
    **Sample Request**

      .. code-block:: sh
        
          curl --location 'https://app.4pointx.com/_config/pipelines/_action' \
               --header 'Content-Type: application/json' \
               --header 'Authorization: Basic <token>' \
               --data '{
                   "id": ["710e6fe9-c6a5-40c6-bcf4-54b6cc26d8ac"],
                   "action": "start"
                  }'

    **Sample Response**

    Success 

      .. code-block:: json
          
        "Action is being queued"

    
    Error 

      .. sourcecode:: json

        {
          "error": {
            "status": 400,
            "message":  "Insufficient Inputs"
          }
        }


          

      .. sourcecode:: json

        {
          "error": {
            "status": 400,
            "message": "Pipeline Not Found: 123698-287381-78348-2819"
          }
        }
      
