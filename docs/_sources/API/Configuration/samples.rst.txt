**Samples**
================


**Retrieve Sample Trend**
++++++++++++++++++++++++++++
.. http:get:: /_config/samples/_trend

    
    **Description** Retrieve the sample trend for functions or pipelines within a specified time range.

    **HTTP Method** GET

    **Endpoint** ``/_config/samples/_trend``

    **Request Header** ``Authorization`` `Basic <token>`

    **Request Parameters**
      
    Query Parameters

      - ``pipeline_id`` (string, *optional*):  The ID of the pipeline for which you want to retrieve the sample trend.
      - ``function_id`` (string, *optional*):   The ID of the function for which you want to retrieve the sample trend.
      - ``from`` (string, *optional*): The start time for the trend data (default: "now-7d").
      - ``to`` (string, *optional*):  The end time for the trend data (default: "now").

    

    **Sample Request**

      .. code-block:: sh

        curl --location 'https://app.4pointx.com/_config/samples/_trend?function_id=45c98f40-75e3-49e7-b178-d25ec878db79&from=2023-08-26T19%3A13%3A00.000Z&to=2023-09-02T19%3A13%3A59.999Z' \
              --header 'Authorization: Basic <token>' \
              --data ''


    **Sample Response**

    Success 

      .. code-block:: json

        [
          {
              "ts": 1693074600000,
              "event_count": 1900
          },
          {
              "ts": 1693161000000,
              "event_count": 1200
          },
          {
              "ts": 1693247400000,
              "event_count": 1800
          }
        ]
            
          

    Error 

      .. sourcecode:: json

          {
            "error": {
              "status": 400,
              "message":  "Insufficient Inputs"
            }
          }

          

      .. sourcecode:: json

          {
            "error": {
              "status": 400,
              "message": "Excess Inputs"
            }
          }
          

        
**Retrieve Sample Count**
+++++++++++++++++++++++++++++
.. http:GET:: /_config/samples/_count

    **Description** Retrieve the total count of samples for a specific function or pipeline.

    **HTTP Method** GET

    **Endpoint** ``/_config/samples/_count``

    **Request Header** ``Authorization`` `Basic <token>`

    **Request Parameters**

    Query Parameters


      - ``pipeline_id`` (string, *optional*): The ID of the pipeline for which you want to retrieve the sample count.
      - ``function_id`` (string, *optional*): The ID of the function for which you want to retrieve the sample count.
      
      
    **Sample Request**

      .. code-block:: sh
        
          curl --location 'https://app.4pointx.com/_config/samples/_count?function_id=3626c23b-2070-48bb-baaa-1170a39c0f88%0A' \
              --header 'Authorization: Basic <token>' \
              --data ''

    **Sample Response**

    Success 

      .. code-block:: json
          
        8

    
    Error 

      .. sourcecode:: json

        {
          "error": {
            "status": 400,
            "message":  "Insufficient Inputs"
          }
        }


          

      .. sourcecode:: json

        {
          "error": {
            "status": 400,
            "message": "Excess Inputs"
          }
        }
      
            


