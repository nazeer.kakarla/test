**Pipelines (Edge Server)**
============================


**Create Edge Server Pipeline**
+++++++++++++++++++++++++++++++++++++
.. http:post:: /_config/pipelines/edge-server/
  
  **Description** This is used for creating a new edge pipeline.

  **HTTP Method** POST
  
  **Endpoint:** ``/_config/pipelines/edge-server/``

  **Request Header** ``Authorization`` `Basic <token>`


  **Request Parameters** 

  Body Parameters


    - ``function_id`` (string, *optional*): ID of the function to which pipeline will be associated.
    - ``plant_id`` (string, *optional*): ID of the plant to which pipeline will be associated.
    - ``pipeline_name`` (string, *required*): The name of the pipeline.
    - ``data_type`` (string, *required*): The type of data the pipeline handles.
    - ``data_source`` (string, *required*): The source of the data.
    - ``data_source_subtype`` (string, *required*): The subtype of the data source.
    - ``settings`` (object, *required*):  Configuration settings for the pipeline.
          
          - ``mac_address`` (string, *required*): The MAC address of the device.
          - Additional parameters based on `data_source_subtype`:

               - If `data_source_subtype` is `OPC-UA`:

                     - ``server_url`` (string, *required*):  The URL of the OPC UA server.
                     - ``user_name`` (string, *optional*):  The username for authentication.
                     - ``user_password`` (string, *optional*):  The password for authentication.
               
               - If `data_source_subtype` is `File`:

                     - ``separator`` (string, *required*):  The separator used to delimit fields in the file. 
                     - ``rows_skipped`` (string, *required*):  The number of rows to skip at the beginning of the file.
                     - ``time_format`` (string, *required*):  The format of the timestamp in the file.
                     - ``ignore_fields`` (array of strings, *required*): Fields to be ignored during processing. 
                     - ``input_file`` (string, *optional*): The name of the input file.
                     - ``file_name`` (string, *required*):  The name of the file to be monitored.
                     - ``file_path`` (string, *required*):  The path to the file.
                     - ``time_field`` (string, *optional*): The field containing timestamp information.
                    
               - If `data_source_subtype` is `Modbus-RTU`:

                     - ``port`` (integer, *required*):  The port used for data communication.
                     - ``stopbit`` (integer, *required*):  The number of stop bits. 
                     - ``bytesize`` (integer, *required*):  The size of data bytes.
                     - ``parity`` (string, *required*): The type of parity used for communication.
                     - ``baudrate`` (integer, *required*): The baud rate for data transfer.
                     - ``sampling_rate`` (integer, *required*):  The rate at which data is sampled.
                     - ``collect_fft`` (boolean, *optional*): Indicates whether the gateway collects the FFT data or not.
                     - ``fft_intervel`` (integer, *optional*): Specifies the time interval for collecting FFT data (in minutes).
                    

    - ``tags`` (array of objects, *required*): Tags associated with the pipeline.
         
          - ``tag_name`` (string, *required*): The name of the tag.
          - ``tag_desc`` (string, *optional*): Description of the tag.
          - ``tag_unit`` (string, *optional*): The unit of measurement for the tag.

              - If `data_source_subtype` is `OPC-UA`:
                     
                     - ``monitor_id`` (string, *required*): The ID of the monitor associated with the tag. 
                     - ``address`` (string, *required*): The address of the tag.                      
                     - ``tag_id`` (string, *optional*): An identifier for the tag.

                    
              - If `data_source_subtype` is `File`:

                     - ``field_name`` (string, *required*): The name of the field in the production data file. 
                     - ``tag_id`` (string, *required*): An identifier for the tag.
                     - ``field_name`` (string, *required*): The ID of the monitor associated with the tag. 
                     - ``registered_by_asset`` (boolean, *optional*):  Indicates whether the tag is registered by an asset.  
                     - ``asset_id`` (string, *optional*):  The ID of the asset associated with the tag. 
                     - ``monitor_id`` (string, *required*):  The ID of the asset associated with the tag. This option appears in case data type is production.
              
              - If `data_source_subtype` is `Modbus-RTU`:

                     - ``address`` (string, *required*): The address of the tag.  
                     - ``decode_bits`` (string, *required*): The number of bits used in the data decoding process.
                     - ``decode_type`` (string, *required*): The format of the data after decoding (e.g., unsigned integer). 
                     - ``scale`` (string, *required*): The factor by which the decoded data value is multiplied for accurate representation.  
                     
    - ``registration`` (array of objects, *optional*): Registration details for monitors. Other attributes are:

          - ``monitor_id`` (string, *optional*): The ID of the monitor associated with the tag.
          - ``asset_id`` (string, *optional*):  The ID of the asset associated with the tag. 
          - ``asset_name`` (string, *optional*):  The name of the asset associated with the tag.  
          - ``slave_id`` (string, *required*): The slave ID.
          - ``address`` (string, *optional*): The address for registration. 
          - ``registered_by_asset`` (boolean, *required*): Indicates whether the tag is registered by an asset..

    - ``function_enabled_in_registration`` (boolean, *optional*): Indicates whetheror not to override gateway's function so that we can assign slaves to different functions.
  


  **Sample Request**

    .. code-block:: sh
      
        curl --location 'https://app.4pointx.com/_config/pipelines/edge-server/' \
             --header 'Content-Type: application/json' \
             --header 'Authorization: Basic <token>' \
             --data '{
                  "pipeline_name":"test gateway 4sep",
                  "site_id":"4230adcb-f349-4780-bf79-4a8c94a3b13b",
                  "plant_id":"7a45d8e5-92d2-46a7-92d3-85aa4bd57bba",
                  "function_id":"2136f450-1ab0-45ed-96fa-fec9c32da324",
                  "data_type":"Condition","data_source":"Edge IoT Gateway",
                  "data_source_subtype":"Modbus RTU",
                  "settings":{"port":"/dev/ttymxc5",
                              "stopbit":"1",
                              "bytesize":"8",
                              "parity":"N",
                              "baudrate":"9600",
                              "mac_address":"asdaskj",
                              "sampling_rate":1},
                  "tags":[
                        {"tag_name":"displacement_x","address":"a","tag_unit":"w","tag_desc":"",decode_bits:"32",…},
                        {"tag_name":"displacement_y","address":"","tag_unit":"","tag_desc":"",decode_bits:"32",…},
                        {"tag_name":"displacement_z","address":"","tag_unit":"","tag_desc":"",decode_bits:"32",…},
                        {"tag_name":"velocity_x","address":"s","tag_unit":"w","tag_desc":"",decode_bits:"32",…},
                        {"tag_name":"velocity_y","address":"","tag_unit":"","tag_desc":"",decode_bits:"32",…},
                        {"tag_name":"velocity_z","address":"","tag_unit":"","tag_desc":"",decode_bits:"32",…},
                        {"tag_name":"acceleration_x","address":"","tag_unit":"","tag_desc":"",decode_bits:"32",…},
                        {"tag_name":"acceleration_y","address":"","tag_unit":"","tag_desc":"",decode_bits:"32",…},
                        {"tag_name":"acceleration_z","address":"","tag_unit":"","tag_desc":"",decode_bits:"32",…},
                        {"tag_name":"total_acceleration","address":"","tag_unit":"","tag_desc":"",decode_bits:"32",…},
                        {"tag_name":"audio","address":"","tag_unit":"","tag_desc":"",decode_bits:"32",…},
                        {"tag_name":"temperature","address":"","tag_unit":"","tag_desc":"",decode_bits:"32",…}],
                        "registration":[
                           {"slave_id":"14",
                           "function_id":"2136f450-1ab0-45ed-96fa-fec9c32da324",
                           "function_name":"test_latest",
                           "registered_by_asset":true,
                           "asset_id":"129a22cb-7006-4260-a2d7-0d17d78e41bf",
                           "asset_name":"trial2",
                           "asset_options":[{"value":"129a22cb-7006-4260-a2d7-0d17d78e41bf","inputDisplay":"trial2"},{"value":"42039075-bc51-4153-b61a-8dfd9b22e4ee","inputDisplay":"trial3"},{"value":"eefbbde6-0ede-4117-b266-4af8640a4eff","inputDisplay":"trial3"},{"value":"ac1c9638-0633-445a-bb3b-5d292508528c","inputDisplay":"test_4"}],"monitor_id":"977db698-de2e-4094-b812-9767ee8089ca","monitor_name":"new_mon_2","monitor_options":[{"value":"977db698-de2e-4094-b812-9767ee8089ca","inputDisplay":"new_mon_2"}]}],
                           "function_enabled_in_registration":false}'
     
  **Sample Response**

  Success 

    .. sourcecode:: json

          {
            "id": "e894a4da-469d-4261-954b-5a9151c096ad"
          }
          

  Error 
    
    .. sourcecode:: json

      {
        "error": {
          "status": 400,
          "message": "Invalid Content Type"
        }
      }
          
          

    .. sourcecode:: json

      {
        "error": {
          "status": 400,
          "message": "Insufficient Inputs"
        }
      }
   
    .. sourcecode:: json

      {
        "error": {
          "status": 400,
          "message": "Mac address already taken pipeline_id:12730-1928-12730-3982"
        }
      }


    .. sourcecode:: json

      {
        "error": {
          "status": 400,
          "message": "Monitor already registered in another pipeline"
        }
      }
          
          
    .. sourcecode:: json

      {
        "error": {
          "status": 400,
          "message": "Invalid Input key"
        }
      }

    .. sourcecode:: json

      {
        "error": {
          "status": 400,
          "message": "Error creating edge pipeline"
        }
      }

    .. sourcecode:: json

      {
        "error": {
          "status": 404,
          "message": "Invalid Input Registration"
        }
      }
        

          
    





**Delete Pipeline**
++++++++++++++++++++
.. http:delete:: /_config/pipelines/edge-server/<pipeline_id>

  **Description** Delete an edge pipeline.

  **HTTP Method** DELETE

  **Endpoint** ``/_config/pipelines/edge-server/<pipeline_id>``

  **Request Header** ``Authorization`` `Basic <token>`


  **Request Parameters**

  URL Parameters

    - ``pipeline_id`` (string, *required*) The ID of the pipeline to be deleted.

  
  **Sample Request**

    .. code-block:: sh
      
      curl --location --request DELETE 'https://app.4pointx.com/_config/pipelines/edge-server/e894a4da-469d-4261-954b-5a9151c096ad' \
           --header 'Authorization: Basic <token>' \
           --data ''

  **Sample Response**

  Success 

    .. sourcecode:: json

        "Pipeline deleted"

  Error 

    .. sourcecode:: json

      {
        "error": {
          "status": 404,
          "message": "Invalid Pipeline 12839-129837-4837-1289"
        }
      }

        
      





**Update Pipeline**
+++++++++++++++++++++
.. http:put:: /_config/pipelines/edge-server/<pipeline_id>
  
    **Description** Update an existing edge pipeline.

    **HTTP Method** PUT
    
    **Endpoint** ``/_config/pipelines/edge-server/<pipeline_id>``

    **Request Header** ``Authorization`` `Basic <token>`

    **Request Parameters**

    Body Parameters
    
        
      - ``function_id`` (string, *optional*): ID of the function to which pipeline will be associated.
      - ``plant_id`` (string, *optional*): ID of the plant to which pipeline will be associated.
      - ``pipeline_name`` (string, *required*): The updated name of the pipeline.
      - ``settings`` (object, *required*):  Configuration settings for the pipeline.
            
            - ``mac_address`` (string, *required*): The MAC address of the device.
            - Additional parameters based on `data_source_subtype`:

                - If `data_source_subtype` is `OPC-UA`:

                      - ``server_url`` (string, *required*):  The URL of the OPC UA server.
                      - ``user_name`` (string, *optional*):  The username for authentication.
                      - ``user_password`` (string, *optional*):  The password for authentication.
                
                - If `data_source_subtype` is `File`:

                      - ``separator`` (string, *required*):  The separator used to delimit fields in the file. 
                      - ``rows_skipped`` (string, *required*):  The number of rows to skip at the beginning of the file.
                      - ``time_format`` (string, *required*):  The format of the timestamp in the file.
                      - ``ignore_fields`` (array of strings, *required*): Fields to be ignored during processing. 
                      - ``input_file`` (string, *optional*): The name of the input file.
                      - ``file_name`` (string, *required*):  The name of the file to be monitored.
                      - ``file_path`` (string, *required*):  The path to the file.
                      - ``time_field`` (string, *optional*): The field containing timestamp information.
                      
                - If `data_source_subtype` is `Modbus-RTU`:

                      - ``port`` (integer, *required*):  The port used for data communication.
                      - ``stopbit`` (integer, *required*):  The number of stop bits. 
                      - ``bytesize`` (integer, *required*):  The size of data bytes.
                      - ``parity`` (string, *required*): The type of parity used for communication.
                      - ``baudrate`` (integer, *required*): The baud rate for data transfer.
                      - ``sampling_rate`` (integer, *required*):  The rate at which data is sampled.
                      

      - ``tags`` (array of objects, *required*): Tags associated with the pipeline.
          
            - ``tag_name`` (string, *required*): The name of the tag.
            - ``tag_desc`` (string, *optional*): Description of the tag.
            - ``tag_unit`` (string, *optional*): The unit of measurement for the tag.

                - If `data_source_subtype` is `OPC-UA`:
                      
                      - ``monitor_id`` (string, *required*): The ID of the monitor associated with the tag. 
                      - ``address`` (string, *required*): The address of the tag. 
                      - ``tag_id`` (string, *optional*): An identifier for the tag.

                      
                - If `data_source_subtype` is `File`:

                      - ``field_name`` (string, *required*): The name of the field in the production data file. 
                      - ``tag_id`` (string, *required*): An identifier for the tag.
                      - ``field_name`` (string, *required*): The ID of the monitor associated with the tag. 
                      - ``registered_by_asset`` (boolean, *optional*):  Indicates whether the tag is registered by an asset.  
                      - ``asset_id`` (string, *optional*):  The ID of the asset associated with the tag. 
                      - ``monitor_id`` (string, *required*):  The ID of the asset associated with the tag. This option appears in case data type is production.
                
                - If `data_source_subtype` is `Modbus-RTU`:

                      - ``address`` (string, *required*): The address of the tag. 
                      - ``decode_bits`` (string, *required*): The number of bits used in the data decoding process.
                      - ``decode_type`` (string, *required*): The format of the data after decoding (e.g., unsigned integer). 
                      - ``scale`` (string, *required*): The factor by which the decoded data value is multiplied for accurate representation.   

      - ``registration`` (array of objects, *optional*): Registration details for monitors. Other attributes are:

          - ``monitor_id`` (string, *optional*): The ID of the monitor associated with the tag. 
          - ``asset_id`` (string, *optional*):  The ID of the asset associated with the tag. 
          - ``asset_name`` (string, *optional*):  The name of the asset associated with the tag.  
          - ``slave_id`` (string, *required*): The slave ID.
          - ``address`` (string, *optional*): The address for registration. 
          - ``registered_by_asset`` (boolean, *required*): Indicates whether the tag is registered by an asset..



    **Sample Request**

      .. code-block:: sh
      
          curl --location --request PUT 'https://app.4pointx.com/_config/pipelines/edge-server/e894a4da-469d-4261-954b-5a9151c096ad' \
               --header 'Content-Type: application/json' \
               --header 'Authorization: Basic <token>' \
               --data '{"pipeline_name":"mod test gateway 4sep",
                        "site_id":"4230adcb-f349-4780-bf79-4a8c94a3b13b",
                        "settings":{"port":"/dev/ttymxc5","stopbit":"1","bytesize":"8","parity":"N","baudrate":"9600","mac_address":"asdaskj","sampling_rate":1},
                        "tags":[{"tag_name":"displacement_x","address":"a","tag_unit":"w","tag_desc":"",decode_bits:"32",…},
                                {"tag_name":"displacement_y","address":"","tag_unit":"","tag_desc":"",decode_bits:"32",…},
                                {"tag_name":"displacement_z","address":"","tag_unit":"","tag_desc":"",decode_bits:"32",…},
                                {"tag_name":"velocity_x","address":"s","tag_unit":"w","tag_desc":"",decode_bits:"32",…},
                                {"tag_name":"velocity_y","address":"","tag_unit":"","tag_desc":"",decode_bits:"32",…},
                                {"tag_name":"velocity_z","address":"","tag_unit":"","tag_desc":"",decode_bits:"32",…},
                                {"tag_name":"acceleration_x","address":"","tag_unit":"","tag_desc":"",decode_bits:"32",…},
                                {"tag_name":"acceleration_y","address":"","tag_unit":"","tag_desc":"",decode_bits:"32",…},
                                {"tag_name":"acceleration_z","address":"","tag_unit":"","tag_desc":"",decode_bits:"32",…},
                                {"tag_name":"total_acceleration","address":"","tag_unit":"","tag_desc":"",decode_bits:"32",…},
                                {"tag_name":"audio","address":"","tag_unit":"","tag_desc":"",decode_bits:"32",…},
                                {"tag_name":"temperature","address":"","tag_unit":"","tag_desc":"",decode_bits:"32",…}],
                                "registration":[{"slave_id":"14","function_id":"2136f450-1ab0-45ed-96fa-fec9c32da324","registered_by_asset":true,"asset_id":"129a22cb-7006-4260-a2d7-0d17d78e41bf",
                                "asset_options":[{"value":"129a22cb-7006-4260-a2d7-0d17d78e41bf","inputDisplay":"trial2"},{"value":"42039075-bc51-4153-b61a-8dfd9b22e4ee","inputDisplay":"trial3"},{"value":"eefbbde6-0ede-4117-b266-4af8640a4eff","inputDisplay":"trial3"},{"value":"ac1c9638-0633-445a-bb3b-5d292508528c","inputDisplay":"test_4"}],
                                "monitor_id":"977db698-de2e-4094-b812-9767ee8089ca","monitor_name":"new_mon_2","monitor_options":[{"value":"977db698-de2e-4094-b812-9767ee8089ca","inputDisplay":"new_mon_2"}]}]}'



    **Sample Response**

    Success

      .. sourcecode:: json
              
              
         "Pipeline Updated"
          
          
                                      

    Error

      .. sourcecode:: json

        {
          "error": {
            "status": 400,
            "message": "Invalid Content Type"
          }
        }

          

      .. sourcecode:: json


        {
          "error": {
            "status": 400,
            "message":  "Insufficient Inputs"
          }
        }


      .. sourcecode:: json

        {
          "error": {
            "status": 400,
            "message":  "Mac address already taken pipeline_id 128937-2189379-12839-1928"
          }
        }
      
      .. sourcecode:: json

        {
          "error": {
            "status": 400,
            "message":  "Monitor already registered in another pipeline"
          }
        }

      


          



**Fetch Pipeline**
+++++++++++++++++++++
.. http:get:: /_config/pipelines/edge-server/<pipeline_id>

    
    **Description** Retrieve details of a specific edge pipeline.


    **HTTP Method** GET


    **Endpoint** ``/_config/pipelines/edge-server/<pipeline_id>``

    **Request Header** ``Authorization`` `Basic <token>`

    **Request Parameters**
      
    Query Parameters

      - ``mac_ids`` (array of strings, *optional*): An array of MAC addresses to identify pipelines.
        
    **Sample Request**

      .. code-block:: sh

          curl --location 'https://app.4pointx.com/_config/pipelines/edge-server/f944e82b-2c2d-4de7-b57d-1d293eabd785' \
               --header 'Authorization: Basic <token>'
    
    **Sample Response**

    Success 

      .. code-block:: json
            
         {
            "function_id": "e6f205ca-89a1-40dd-891e-3adc4d0f80d5",
            "plant_id": "55297f65-06a2-490e-bd34-464611ac10b0",
            "pipeline_id": "f944e82b-2c2d-4de7-b57d-1d293eabd785",
            "created_at": 1705904884634,
            "data_type": "Condition",
            "data_source": "Edge IoT Gateway",
            "pipeline_name": "a_demo",
            "status": "starting",
            "total_event_count": 0,
            "data_source_subtype": "Modbus RTU",
            "data_directory": "/home/debian/data",
            "site_id": "d2e66edd-f13f-4d00-a7b5-8cab9ef4488d",
            "settings": {
                "port": "/dev/ttymxc5",
                "stopbit": "1",
                "bytesize": "16",
                "parity": "N",
                "baudrate": "9600",
                "mac_address": "asdfkga",
                "sampling_rate": 4,
                "ip": ""
            },
            "tags": [
              {
                  "address": "a",
                  "tag_name": "displacement_y",
                  "tag_desc": "",
                  "tag_unit": "",
                  "decode_bits": "32",
                  "decode_type": "unsign_int",
                  "scale": "1"
              },
              {
                  "address": "a",
                  "tag_name": "displacement_z",
                  "tag_desc": "",
                  "tag_unit": "",
                  "decode_bits": "32",
                  "decode_type": "unsign_int",
                  "scale": "1"
              },
              {
                  "address": "a",
                  "tag_name": "velocity_x",
                  "tag_desc": "",
                  "tag_unit": "",
                  "decode_bits": "32",
                  "decode_type": "unsign_int",
                  "scale": "1"
              },
              {
                  "tag_unit": "as",
                  "address": "sadf",
                  "tag_name": "velocity_x",
                  "tag_desc": "",
                  "decode_bits": "32",
                  "decode_type": "unsign_int",
                  "scale": "1"
              },
              {
                  "tag_unit": "",
                  "address": "",
                  "tag_name": "velocity_y",
                  "tag_desc": "",
                  "decode_bits": "32",
                  "decode_type": "unsign_int",
                  "scale": "1"
              },
              {
                  "tag_unit": "",
                  "address": "",
                  "tag_name": "velocity_z",
                  "tag_desc": "",
                  "decode_bits": "32",
                  "decode_type": "unsign_int",
                  "scale": "1"
              },
              {
                  "tag_unit": "",
                  "address": "",
                  "tag_name": "acceleration_x",
                  "tag_desc": "",
                  "decode_bits": "32",
                  "decode_type": "unsign_int",
                  "scale": "1"
              },
              {
                  "tag_unit": "",
                  "address": "",
                  "tag_name": "acceleration_y",
                  "tag_desc": "",
                  "decode_bits": "32",
                  "decode_type": "unsign_int",
                  "scale": "1"
              },
              {
                  "tag_unit": "",
                  "address": "",
                  "tag_name": "acceleration_z",
                  "tag_desc": "",
                  "decode_bits": "32",
                  "decode_type": "unsign_int",
                  "scale": "1"
              },
              {
                  "tag_unit": "",
                  "address": "",
                  "tag_name": "total_acceleration",
                  "tag_desc": "",
                  "decode_bits": "32",
                  "decode_type": "unsign_int",
                  "scale": "1"
              },
              {
                  "tag_unit": "",
                  "address": "",
                  "tag_name": "audio",
                  "tag_desc": "",
                  "decode_bits": "32",
                  "decode_type": "unsign_int",
                  "scale": "1"
              },
              {
                  "tag_unit": "",
                  "address": "",
                  "tag_name": "temperature",
                  "tag_desc": "",
                  "decode_bits": "32",
                  "decode_type": "unsign_int",
                  "scale": "1"
              }
            ],
            "registration": [
                {
                    "monitor_id": "5477c2a2-2ed3-459c-9a7b-8a14d623d41f",
                    "slave_id": 14
                }
            ],
            "watcher_id": "71fa5359-bbad-42db-816d-47a94d366461",
            "last_action": {
                "action_id": "555f645e-6152-44ff-b1f5-fa2b00627c96",
                "action": "start",
                "status": "in_progress",
                "issued_at": 1705911449386,
                "completed_at": null,
                "pipeline_id": "f944e82b-2c2d-4de7-b57d-1d293eabd785"
            }
         }

    Error 

      .. sourcecode:: json

        {
          "error": {
            "status": 400,
            "message":  "Invalid Pipeline 128937-32948-217360-3476"
          }
        }

          






**Handshake**
+++++++++++++++++++++++++++++
.. http:GET:: /_config/pipelines/edge-server/handshake/

    **Description** Perform a handshake with the edge server to get connection information.

    **HTTP Method** POST

    **Endpoint** ``/_config/pipelines/edge-server/handshake/``

    **Request Header** ``Authorization`` `Basic <token>`

    **Request Parameters**

    Body Parameters


      - ``mac_ids`` (string, *optional*): A comma-separated list of MAC addresses for the devices. This is used to identify the pipelines associated with these devices.
     
    **Sample Request**

      .. code-block:: sh
        
          curl --location 'https://app.4pointx.com/_config/pipelines/edge-server/handshake' \
               --header 'Content-Type: application/json' \
               --header 'Authorization: Basic <token>' \
               --data '{"mac_ids": ["00:11:22:33:44:55"]}'

    **Sample Response**

    Success 

      .. code-block:: json
          
        {
            "kafka_username": "sample",
            "kafka_password": "12376912873ashjow83109",
            "pipeline_id": "3922a7cd-b504-4438-a9c7-30b5366fb171",
            "nginx_token": "Basic asl2F0ZXdheadcjdhdmWHhzWnJNajRXbmJTY0pEUFE5QnRuSG9nRFphcw==",
            "timezone": "Asia/Kolkata"
         }

    
    Error 

      .. sourcecode:: json

        {
          "error": {
            "status": 400,
            "message":  "No Pipelines found for: 00:11:22:33:44:55"
          }
        }





**Firmware**
+++++++++++++++++++++++++++
.. http:get:: /_config/pipelines/edge-server/_firmware/
      
  **Description** pipelines/edge-server/_firmware/ of assets based on various criteria such as function, asset category, and criticality.


  **HTTP Method** GET


  **Endpoint** ``/_config/pipelines/edge-server/_firmware/``

  **Request Header** ``Authorization`` `Basic <token>`

    
  **Request Parameters**

  Query Parameters


    - ``site_id`` ( string, *optional*): The ID of the site for which to retrieve firmware.
    - ``version`` (string, *optional*): The specific version of the firmware to retrieve.


  **Sample Request**

    ..  code-block:: sh
      
        curl --location 'https://app.4pointx.com/_config/pipelines/edge-server/_firmware?site_id=4230adcb-f349-4780-bf79-4a8c94a3b13b' \
             --header 'Authorization: Basic <token>' \
             --data ''
    
  **Sample Response**

  Success 

    .. sourcecode:: text

          A downloadable firmware file.

  Error 

  
    .. sourcecode:: json

      {
        "error": {
          "status": 400,
          "message":  "Excess Inputs"
        }
      }


    .. sourcecode:: json

      {
        "error": {
          "status": 400,
          "message":  "Insufficient Inputs"
        }
      }
