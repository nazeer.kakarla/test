**Assets**
================


**Create Asset**
++++++++++++++++++++
.. http:post:: /_config/assets/
  
  **Description** This is used for creating new asset.

  **HTTP Method** POST
  
  **Endpoint:** ``/_config/assets/``

  **Request Header** ``Authorization`` `Basic <token>`


  **Request Parameters** 

  Body Parameters



    - ``asset_name`` (string, *required*): The name of the asset.
    - ``function_id`` (string, *required*): The ID of the associated function.
    - ``plant_id`` (string, *optional*): The ID of the associated plant (default is determined from the function).
    - ``site_id`` (string, *optional*): The ID of the associated site (default is determined from the plant).
    - ``asset_category`` (string, *required*): The category of the asset (e.g., "Rotating" or "Stationary").
    - ``is_critical`` (string, *optional*): Indicates whether the asset is critical ("yes" or "no", default is "no").
    - ``attributes`` (object, *optional*): Additional attributes specific to the asset.

          - ``machine_type`` (string, *optional*): The type or category of the machine (eg: motor, pump, fan etc).
          - ``rotating_speed`` (number, *optional*): The rotating speed of the asset, applicable for rotating machinery (e.g., RPM for a motor).
          - ``manufacturer`` (string, *optional*): The manufacturer or company that produced the asset.
          - ``machine_model`` (string, *optional*): The model or version of the machine.
          - ``serial_no`` (string, *optional*): Serial or unique identifier for the asset.
          - ``phases`` (string, *optional*): The number of phases the asset operates in.
          - ``kw`` (number, *optional*):  The power rating of the asset in kilowatts (kW).
          - ``frequency`` (number, *optional*): The frequency at which the asset operates (in Hertz, Hz).
          - ``volts`` (number, *optional*): The voltage rating of the asset (in volts, V).
          - ``amp`` (number, *optional*): The current rating of the asset in amperes (Amps, A).

  


  **Sample Request**

    .. code-block:: sh
      
        curl --location 'https://app.4pointx.com/_config/assets/' \
        --header 'Content-Type: application/json' \
        --header 'Authorization: Basic <token>' \
        --data '{"asset_name": "demo-asset2-test",
                "asset_category": "All",
                "function_id": "45c98f40-75e3-49e7-b178-d25ec878db79",
                "is_critical": "no",
                "attributes": {
                    "machine_type": "motor",
                    "rotating_speed": 0,
                    "manufacturer": "",
                    "serial_no": "",
                    "phases": "single-phase",
                    "kw": 0,
                    "frequency": 0,
                    "volts": 0,
                    "amp": 0
                }
        }'
                
     
  **Sample Response**

  Success 

    .. sourcecode:: json

          {
              "id": "290e3731-b19e-4f04-8a71-7c724ad7c2e6"
          }
          

  Error 
    
    .. sourcecode:: json

      {
        "error": {
          "status": 400,
          "message": "Invalid Content Type"
        }
      }
          
          

    .. sourcecode:: json

      {
        "error": {
          "status": 400,
          "message": "Insufficient Inputs"
        }
      }

          
          
    .. sourcecode:: json

      {
        "error": {
          "status": 400,
          "message": "Invalid asset data"
        }
      }
        

          
    





**Delete Asset**
++++++++++++++++++++
.. http:delete:: /_config/assets/<asset_id>

  **Description** Delete an asset by its ID. An asset cannot be deleted if it has monitors attached to it.

  **HTTP Method** DELETE

  **Endpoint** ``/_config/assets/<asset_id>``

  **Request Header** ``Authorization`` `Basic <token>`


  **Request Parameters**

  URL Parameters

    - ``asset_id`` (string, *required*) The ID of the asset to delete.

  
  **Sample Request**

    .. code-block:: sh
      
      curl --location --request DELETE 'https://app.4pointx.com/_config/assets/290e3731-b19e-4f04-8a71-7c724ad7c2e6' \
            --header 'Authorization: Basic <token>' \
            

  **Sample Response**

  Success 

    .. sourcecode:: text

        "Asset deleted"

  Error 

    .. sourcecode:: json

      {
        "error": {
          "status": 400,
          "message": "Asset can't be deleted, monitors are attached"
        }
      }

        
      





**Update Asset**
+++++++++++++++++++++
.. http:put:: /_config/assets/<asset_id>
  
    **Description** Update the details of an existing asset.

    **HTTP Method** PUT
    
    **Endpoint** ``/_config/assets/<asset_id>``

    **Request Header** ``Authorization`` `Basic <token>`

    **Request Parameters**

    Body Parameters
    
      - ``asset_name`` (string, *required*): The name of the asset.
      - ``function_id`` (string, *required*): The ID of the associated function.
      - ``plant_id`` (string, *optional*): The ID of the associated plant (default is determined from the function).
      - ``site_id`` (string, *optional*): The ID of the associated site (default is determined from the plant).
      - ``asset_category`` (string, *required*): The category of the asset (e.g., "Rotating" or "Stationary").
      - ``is_critical`` (string, *required*): Indicates whether the asset is critical ("yes" or "no").
      - ``attributes`` (object, *optional*): Additional attributes specific to the asset.

          - ``machine_type`` (string, *optional*): The type or category of the machine (eg: pump,fan,motor).
          - ``rotating_speed`` (number, *optional*): The rotating speed of the asset, applicable for rotating machinery (e.g., RPM for a motor).
          - ``manufacturer`` (string, *optional*): The manufacturer or company that produced the asset.
          - ``machine_model`` (string, *optional*): The model or version of the machine.
          - ``serial_no`` (string, *optional*): Serial or unique identifier for the asset.
          - ``phases`` (string, *optional*): The number of phases the asset operates in.
          - ``kw`` (number, *optional*):  The power rating of the asset in kilowatts (kW).
          - ``frequency`` (number, *optional*): The frequency at which the asset operates (in Hertz, Hz).
          - ``volts`` (number, *optional*): The voltage rating of the asset (in volts, V).
          - ``amp`` (number, *optional*): The current rating of the asset in amperes (Amps, A).




    URL Parameters

      - ``asset_id`` (string, *required*) The ID of the asset to update.


    **Sample Request**

      .. code-block:: sh
      
          curl --location --request PUT 'https://app.4pointx.com/_config/assets/3b8e77ad-feee-43ae-bd2e-ebbba8adafc3' \
                --header 'Content-Type: application/json' \
                --header 'Authorization: Basic <token>' \
                --data '{"asset_name":"mod-demo-asset2-test","asset_category":"All","function_id":"45c98f40-75e3-49e7-b178-d25ec878db79",
                "is_critical":
                "no","attributes":
                      {"machine_type":"motor",
                      "rotating_speed":0,
                      "manufacturer":"",
                      "serial_no":"",
                      "phases":"single-phase",
                      "kw":0,
                      "frequency":10,
                      "volts":0,
                      "amp":0}
                }'

    **Sample Response**

    Success

      .. sourcecode:: json
              
              
         "Asset Updated"
          
          
                                      

    Error

      .. sourcecode:: json

        {
          "error": {
            "status": 400,
            "message": "Invalid Content Type"
          }
        }

          

      .. sourcecode:: json


        {
          "error": {
            "status": 400,
            "message":  "Insufficient Inputs"
          }
        }


      .. sourcecode:: json

        {
          "error": {
            "status": 400,
            "message":  "Invalid asset data"
          }
        }


          






**Fetch Asset**
+++++++++++++++++++++
.. http:get:: /_config/assets/<asset_id>

    
    **Description** Retrieve details of a specific asset by its ID.


    **HTTP Method** GET


    **Endpoint** ``/_config/assets/<asset_id>``

    **Request Header** ``Authorization`` `Basic <token>`

    **Request Parameters**
      
    URL Parameters

      - ``asset_id`` (string, *required*): The ID of the asset to retrieve details for.
    

    **Sample Request**

      .. code-block:: sh

          curl --location 'https://app.4pointx.com/_config/assets/547618d4-c481-48e5-b2a0-ae810a94883a' \
          --header 'Authorization: Basic <token>' -k

    **Sample Response**

    Success 

      .. code-block:: json
            
          {
              "asset_id": "547618d4-c481-48e5-b2a0-ae810a94883a",
              "asset_name": "Demo Asset",
              "function_name": "Demo Function",
              "plant_name": "Demo Plant",
              "site_name": "Demo Site",
              "attributes": {},
              "asset_category": "All",
              "is_critical": "No"
          }

    Error 

      .. sourcecode:: json

        {
          "error": {
            "status": 400,
            "message":  "Invalid asset 5476481-48e5-b2a0-ae810a948839"
          }
        }

          






**Fetch List of Assets**
+++++++++++++++++++++++++++++
.. http:GET:: /_config/assets/_list

    **Description** Retrieve a list of assets based on various criteria such as function, asset category, and criticality.

    **HTTP Method** GET

    **Endpoint** ``/_config/assets/_list``

    **Request Header** ``Authorization`` `Basic <token>`

    **Request Parameters**

    Query Parameters


      - ``function_id`` (string, *optional*): Filter assets by function ID.
      - ``asset_category`` (string, *optional*): Filter assets by asset category (e.g., "Rotating" or "Static").
      - ``is_critical`` (string, *optional*): Filter assets by criticality ("Yes" or "No").

    **Sample Request**

      .. code-block:: sh
        
          curl --location 'https://app.4pointx.com/_config/assets/_list?function_id=3626c23b-2070-48bb-baaa-1170a39c0f88&asset_category=All' \
          --header 'Authorization: Basic <token>' \
          

    **Sample Response**

    Success 

      .. code-block:: json
          
        [
            {
                "asset_id": "c828083f-ba1e-4cac-a9f5-5f1424b88db3",
                "asset_name": "Pickling Tank 1",
                "is_critical": "Yes",
                "asset_category": "Rotating",
                "function_name": "PLTCM",
                "plant_name": "CRM-2",
                "site_name": "JSW VJNR",
                "monitor_count": 1
            },
            {
                "asset_id": "74f09f80-0b9a-4e6a-bde1-4188247cf90d",
                "asset_name": "Pickling Tank 2",
                "is_critical": "Yes",
                "asset_category": "Rotating",
                "function_name": "PLTCM",
                "plant_name": "CRM-2",
                "site_name": "JSW VJNR",
                "monitor_count": 2
            },
            {
                "asset_id": "121dfadc-c74b-491e-9b0e-ef0acbc9f329",
                "asset_name": "Mill Stand 4",
                "is_critical": "Yes",
                "asset_category": "Rotating",
                "function_name": "PLTCM",
                "plant_name": "CRM-2",
                "site_name": "JSW VJNR",
                "monitor_count": 6
            },
            {
                "asset_id": "420c0c6c-cba4-47c1-9150-0b2600c5eb64",
                "asset_name": "Mill Stand 2",
                "is_critical": "Yes",
                "asset_category": "Rotating",
                "function_name": "PLTCM",
                "plant_name": "CRM-2",
                "site_name": "JSW VJNR",
                "monitor_count": 7
            },
            {
                "asset_id": "e48edd64-66c7-4130-b325-7b75b40ee934",
                "asset_name": "Mill Stand 3",
                "is_critical": "Yes",
                "asset_category": "Rotating",
                "function_name": "PLTCM",
                "plant_name": "CRM-2",
                "site_name": "JSW VJNR",
                "monitor_count": 6
            }
        ]


            



            





**Get Count of Assets**
+++++++++++++++++++++++++++
.. http:get:: /_config/assets/_count
      
  **Description** Get the count of assets based on various criteria such as function, asset category, and criticality.


  **HTTP Method** GET


  **Endpoint** ``/_config/assets/_count``

  **Request Header** ``Authorization`` `Basic <token>`

    
  **Request Parameters**

  Query Parameters


    - ``function_id`` ( string, *optional*): Filter assets by function ID.
    - ``asset_category`` (string, *optional*): Filter assets by asset category (e.g., "Rotating" or "Stationary").
    - ``is_critical`` (string, *optional*): Filter assets by criticality ("Yes" or "No").


  **Sample Request**

    ..  code-block:: sh
      
        curl --location 'https://app.4pointx.com/_config/assets/_count?function_id=3626c23b-2070-48bb-baaa-1170a39c0f88&asset_category=All' \
        --header 'Authorization: Basic <token>' \
        
    
  **Sample Response**

  Success 

    .. sourcecode:: json

          2



 
          