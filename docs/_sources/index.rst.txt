#####################
4PointX Documentation
#####################

.. toctree::

	4pointx_docs/introduction/index
	4pointx_docs/getting_started_with_4pointx_cloud/index
	4pointx_docs/deploy_4pointx_self_managed/index
	4pointx_docs/apps/index
	4pointx_docs/dashboards/index
	4pointx_docs/alerting/index
	4pointx_docs/notebooks/index
	4pointx_docs/data_management/index
	4pointx_docs/administration/index
	4pointx_docs/partners/index
	4pointx_docs/test_page_for_sphinix_converter/index

.. toctree::
	:caption: API Documentation

	API/Asset_Metadata/index
	API/EMS/index
	API/Notebooks/index
	API/Alerting/index
	API/User_Management/index
	API/Condition_Monitoring/index
	API/License_Management/index












































