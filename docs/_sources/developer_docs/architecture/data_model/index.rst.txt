##########
Data Model
##########


.. raw:: html

	<p>Below is the data model diagram for 4PointX IAoT Workbench and 4PointX Apps. It contains the Indices, Fields and Foreign-key relationships.</p>

.. raw:: html

	<img src='/docs/_static/media/4PointX Data Model-Page-1.drawio (1).png'>

.. toctree::

	data_model_json/index
	sample_test_data/index