##############################
How to get authorization token
##############################


.. raw:: html

	<ul><li><p>Login to environment with admin user and password (dev2, build, test , demo )</p></li><li><p>open another Tab go to link  {hostname}<a href="http://dev2.4pointx.com/api/token">/api/token</a> for example (<a href="http://dev2.4pointx.com/api/token">http://dev2.4pointx.com/api/token</a>)</p></li><li><p>You will see a JSON block , from which you will get token like this <br/></p></li></ul>

.. raw:: html

	<img src='/docs/_static/media/image-20220923-053302.png'>

.. toctree::
