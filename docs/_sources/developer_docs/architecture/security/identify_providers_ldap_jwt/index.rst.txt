##############################
Identify Providers (LDAP, JWT)
##############################


.. raw:: html

	<p>This page provides the implementation details regarding user authentication and authorisation mechanism.</p>



.. raw:: html

	<p>Architecture diagram depicting the interaction b/w various components: LDAP, ES, JWT, APIs, Kibana, API-only user.</p>

.. raw:: html

	<p><ac:link><ri:user ri:userkey="8a7f808a7d0283a6017d03c280bd0176"></ri:user></ac:link> pls create the diagram using <ac:link><ri:page ri:content-title="Cloud Tools" ri:space-key="TC" ri:version-at-save="7"></ri:page><ac:link-body>draw.io</ac:link-body></ac:link> </p>

.. toctree::
