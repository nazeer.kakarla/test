#####
RnD-3
#####




.. list-table::
	:header-rows: 1

	* - Cloud
	  - GCP (accounts@4pointx.com)
	* - Instance Name
	  - rnd-3
	* - IP
	  - 34.134.100.8 (Old IP: 34.172.95.233)
	* - DNS Name
	  - rnd-3.4pointx.com
	* - SSH Key
	  - 4px@rnd-3.pem
	* - Configuration
	  - e2-standard-4 (4 vCPUs, 16 GB memory), 40 GB SSD, 6 GB Swap
	* - Network Tags (F/W Ports)
	  - Ingress Allow:https 443mysql 3306ss-leaf 3311ssh 22testing-port 3399Egress Allow:allow-egress-project-vm (all ports)allow-egress-only-select-ports 80, 443, 22Egress Deny:block-all-outgoing (all ports)



How to SSH into the VM
======================

.. code-block:: text

	chmod 400 4px@rnd-3.pem
	ssh -i 4px@rnd-3.pem 4px@rnd-3.4pointx.com


.. raw:: html

	<a href='/docs/_static/media/4px@rnd-3.pem' class='media-group'><div class='image-preview'><svg width="24" height="24" viewBox="0 0 24 24" role="presentation"><path fill="#758195" fill-rule="evenodd" d="M12 4H8a2 2 0 00-2 2v12a2 2 0 002 2h8a2 2 0 002-2v-6.995h-2V18H8V6h4v2a2 2 0 002 2h3.5a.5.5 0 00.5-.5V8.213a.5.5 0 00-.145-.352L14.61 4.59A2 2 0 0013.19 4H12zM3 0h18a3 3 0 013 3v18a3 3 0 01-3 3H3a3 3 0 01-3-3V3a3 3 0 013-3z"></path></svg></div><div class='filename'>4px@rnd-3.pem</div></a>

.. toctree::
