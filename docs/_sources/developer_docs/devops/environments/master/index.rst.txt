######
Master
######


.. raw:: html

	<p>This is the new Master (PKA Demo) environment based on Swarm. We will be switching to this new env from XX Sep 2023, on wards. <ac:link><ri:page ri:content-title="Demo (non-Swarm)" ri:version-at-save="15"></ri:page><ac:link-body>Old</ac:link-body></ac:link> Demo env is still available for a few more days until we stabilise the new one and repoint POC gateways to new IP.</p>



.. raw:: html

	<p><a href="https://master.4pointx.com">https://master.4pointx.com</a></p>



**********
SaaS Admin
**********

.. raw:: html

	<p><span style="color: rgb(255,86,48);">Restrain from using this ID, instead use Site Admins below</span></p>

.. raw:: html

	<p><code>admin</code> | <code>Ng2v1ZAEMZMzWn3fUP0mmrue4sGo4iPx</code></p>

***********
Site Admins
***********

.. raw:: html

	<p>User Id | Password</p>

.. raw:: html

	<p><code>demo.analyst@4pointx.com</code> | <code>demo.analyst@329</code><br/><code>demo.admin@4pointx.com</code> | <code>demo.admin@329</code></p>



.. raw:: html

	<p><a href="https://es01:9200">https://es01:9200</a></p>



.. raw:: html

	<p>Docker monitoring and control. Users: DevOps</p>

.. raw:: html

	<p><a href="https://master.4pointx.com:9443/">https://master.4pointx.com:9443/</a></p>

.. raw:: html

	<p>user: admin<br/>pwd: <code>qJfFHgcHyHNnVU1r0YK8MfNVedyVnGu8</code></p>

.. raw:: html

	<p>License Key (Business Edition, 5-node): Not sure <span style="color: rgb(255,86,48);">(?)</span></p>

.. raw:: html

	<p><code>2-2Zb0rjC6Bl0YEiRQm1prZG8mTn6Jr3B8jXODgCxF17KOOnxyosoghbdv7jTcelbRh4BimC7yQArPj+M+esU=</code></p>

*******
Grafana
*******

.. raw:: html

	<p>System monitoring. Uses Prometheus. Data comes from node-exporter (container usage data) and cadvisor (vm resources usage)</p>

.. raw:: html

	<p><a href="http://master.4pointx.com:3000/dashboards">http://master.4pointx.com:3000/dashboards</a></p>

.. raw:: html

	<p>user: admin<br/>pwd: <code>4px_admin_329</code></p>

.. raw:: html

	<p>Pwd hard coded in <a href="https://gitlab.com/everlytics/4px.setup/-/blame/test/dockerfiles/monitoring_tools/docker-stack.yml#L114">https://gitlab.com/everlytics/4px.setup/-/blame/test/dockerfiles/monitoring_tools/docker-stack.yml#L114</a></p>

**************************
Redpanda (previously Kowl)
**************************

.. raw:: html

	<p>For monitoring Kafka Topics and Kafka Consumers</p>

.. raw:: html

	<p><a href="http://master.4pointx.com:8080/">http://master.4pointx.com:8080/</a></p>



.. list-table::
	:header-rows: 1

	* - Cloud
	  - GCP (info@4pointx.com)
	* - Instance Name
	  - master-swarm
	* - IP
	  - 34.125.146.154
	* - DNS Name
	  - master.4pointx.com
	* - SSH Key
	  - 4px@master-swarm.pem
	* - Configuration
	  - e2-custom (6 vCPUs, 48 GB memory), 20 GB SSD - OS, 100 GB SSD - Data, X GB Swap



How to SSH into the VM
======================

.. code-block:: text

	chmod 400 4px@master-swarm.pem
	ssh -i 4px@master-swarm.pem 4px@master.4pointx.com


.. raw:: html

	<p>Ask Kiran if you need the SSH key</p>

.. toctree::
