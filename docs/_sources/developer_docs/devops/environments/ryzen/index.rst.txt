#####
Ryzen
#####




.. raw:: html

	<p>https://ryzen.4pointx.com (DNS still not enabled)</p>

.. list-table::
	:header-rows: 1

	* - UserId
	  - Pwd
	  - Role
	  - Permissions
	* - admin
	  - admin
	  - 
	  - 
	* - 
	  - 
	  - 
	  - 




.. raw:: html

	<p><a href="https://dashboard.ngrok.com/get-started/setup">https://dashboard.ngrok.com/get-started/setup</a></p>

.. list-table::
	:header-rows: 1

	* - Account Userame
	  - Password
	* - ryzen@4pointx.com
	  - wisjoz-Wysduz-vazso1




.. raw:: html

	<p>Note: This is a Physical Machine (on-premises at Everlytics office)</p>

.. list-table::
	:header-rows: 1

	* - Location
	  - Everlytics Office (Bangalore)
	* - Instance Name
	  - ryzen
	* - IP
	  - Local: 192.168.1.44External: NAVPN IP: 10.8.0.6 (If you login to Everlytics VPN, you can access the server using this IP)
	* - DNS Name
	  - ryzen.4pointx.com (still not activated)
	* - SSH Key
	  - 4px@ryzen.pemPlease ask Kiran if you need the key for any troubleshooting
	* - Configuration
	  - 8-core CPU, 64 GB memory1x 400GB SSD (/)0 GB Swap



How to SSH into the Machine 
============================

.. raw:: html

	<p><code>chmod 400 4px@ryzen.pem</code></p>

.. raw:: html

	<p><code>ssh -i 4px@ryzen.pem 4px@0.tcp.in.ngrok.io -p &lt;ngrok port&gt;</code></p>

.. toctree::
