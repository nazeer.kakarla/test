#####
Utils
#####






***********************
Install Ngrok on ARM-64
***********************

.. code-block:: bash

	wget https://bin.equinox.io/c/bNyj1mQVY4c/ngrok-v3-stable-linux-arm64.tgz
	sudo tar xvzf ngrok-v3-stable-linux-arm64.tgz -C /usr/local/bin


.. raw:: html

	<p>Detail instructions: <a href="https://ngrok.com/download">https://ngrok.com/download</a></p>

**************************************************
How to Setup ngrok.service (start ngrok on reboot)
**************************************************

.. raw:: html

	<p>Step 1: Create ngrok.yml</p>

.. code-block:: text

	vi /home/4px/.config/ngrok/ngrok.yml


.. code-block:: none

	version: "2"
	authtoken: "<authtoken>"
	tunnels:
	 ssh:
	  proto: tcp
	addr: 22


.. raw:: html

	<p>Step 2: Create ngrok.service</p>

.. code-block:: text

	vi /etc/systemd/system/ngrok.service


.. code-block:: text

	[Unit]
	Description=ngrok
	After=network.target
	
	[Service]
	ExecStart= /usr/local/bin/ngrok start --all --config /home/4px/.config/ngrok/ngrok.yml
	ExecReload=/bin/kill -HUP $MAINPID
	KillMode=process
	IgnoreSIGPIPE=true
	Type=simple
	
	[Install]
	WantedBy=multi-user.target


.. raw:: html

	<p>Step 3: Run the following to enable and link the service</p>

.. code-block:: bash

	systemctl enable ngrok
	systemctl start ngrok




.. code-block:: text

	wget -O speedtest-cli https://raw.githubusercontent.com/sivel/speedtest-cli/master/speedtest.py
	chmod +x speedtest-cli
	./speedtest-cli


.. raw:: html

	<p>Detailed guide is <a href="https://www.cyberciti.biz/faq/install-speedtest-cli-on-centos-redhat-fedoa-scientific-to-measure-internetspeed/">here</a></p>

.. toctree::
