#######
Dockers
#######


************
Introduction
************

.. raw:: html

	<p>This document describes the design of dockerizing the 4PointX platform tech stack. Knowledge about <ac:link><ri:page ri:content-title="Architecture" ri:version-at-save="2"></ri:page><ac:link-body>4PointX technical architecture</ac:link-body></ac:link>, data flow, base platform &amp; modules is required to fully understand the document.</p>

**********
Background
**********

.. raw:: html

	<p>Idea is to containerize all the 4PointX platform technical stack components so that the platform is portable, scalable, and consistent. Dockerizing the stack has been divided into two phases.</p>


Phase-1 Single node deployment (Completed)
==========================================

.. raw:: html

	<ul><li><p>In this phase, all the services will have docker images created. </p></li><li><p>The platform can be deployed on a single server. </p></li><li><p>No orchestration of the services. </p></li><li><p>CI/CD pipeline.</p></li></ul>


Phase-2 Mutli-node setup (Swarm-managed cluster)
================================================

*********************
List of Docker images
*********************

.. raw:: html

	<p>Here is the list of docker images created:</p>

.. list-table::
	:header-rows: 1

	* - S No.
	  - Docker Image
	  - Category
	  - Function
	  - Source Control
	* - 1
	  - OpenSearch and OpenSearch Dashboard (OSD)
	  - Platform
	  - Storage engine and GUI
	  - Amazon
	* - 2
	  - Nginx
	  - Platform
	  - Proxy server
	  - F5 Networks
	* - 3
	  - Kafka Broker and Zookeeper
	  - Platform
	  - Data ingestion cache
	  - Apache
	* - 4
	  - Consumer
	  - Platform
	  - Read data (from #3), process  it and store it in #1
	  - 4PointX
	* - 5
	  - JupyterHub & JupyterLab
	  - Platform
	  - Workbench for custom code scripts and ML models
	  - Project Jupyter
	* - 6
	  - Notebooks
	  - App
	  - GUI for #5
	  - 4PointX
	* - 7
	  - Scheduler
	  - Platform
	  - Used by Notebooks (#6) and other Apps (#7, #8, #9)
	  - 4PointX
	* - 8
	  - Config
	  - App
	  - Metadata configuration
	  - 4PointX
	* - 9
	  - EMS
	  - App
	  - Energy Monitoring System
	  - 4PointX
	* - 10
	  - PdM
	  - App
	  - Process Anomaly/ Performance Digital Twin
	  - 4PointX
	* - 11
	  - CM
	  - App
	  - Condition Monitoring
	  - 4PointX
	* - 12
	  - ML
	  - App
	  - AI Models & Monitors
	  - 4PointX


.. raw:: html

	<p>Each docker image will have a Docker-Compose file (script) that contains instructions on how to build a Docker image. These instructions are, in fact, a group of commands executed automatically in the Docker environment to build a specific Docker image.</p>

.. note::

	The 3rd party dockers (OpenSearch, Kafka, Nginx) are also built and pushed to 4PointX Docker registry instead of pulling from public/ OEM registry. This is to avoid dependency and also enable complete offline deployment.


***********************************
4PointX docker architecture diagram
***********************************

.. raw:: html

	<img src='/docs/_static/media/4PointX docker architecture diagram.png'>



*********
CICD Repo
*********

.. raw:: html

	<p>We don’t use Jenkins or other CICD software. We have our own Python scripts (cicd.py and puller.py) that do CI &amp; CD respectively.</p>

.. raw:: html

	<ul><li><p><strong>GitLab repo:</strong> 4px.cicd <a href="https://gitlab.com/everlytics/4px.cicd">https://gitlab.com/everlytics/4px.cicd</a> </p></li><li><p><strong>CI script:</strong> cicd.py - does the same job as Jenkins (builds the dockers). It exposes an API for building the docker image for each 4PointX module separately. The API is called in .gitlab-ci.yml file present in each module’s repo</p></li><li><p><strong>CD script:</strong> puller.py</p></li><li><p><strong>Dockerfiles location:</strong> 4px.cicd/dockerfiles. We plan to move them to the respective modules repo.</p></li></ul>

***************
Reference links
***************


4PointX Docker architecture & CICD flow diagram
===============================================

.. raw:: html

	<p><a data-card-appearance="inline" href="https://drive.google.com/file/d/1lCKrFHeBrm869vm3xHOxR_ll-ifs_hKp/view?usp=sharing">https://drive.google.com/file/d/1lCKrFHeBrm869vm3xHOxR_ll-ifs_hKp/view?usp=sharing</a> </p>

.. raw:: html

	<p><em>(Shared drives &gt; Modules 4PointX &gt; Docs &gt; 4pointX Docker Flow.drawio)</em></p>

.. raw:: html

	<p>Edit it using: <a href="https://app.diagrams.net">https://app.diagrams.net</a></p>


4PointX Docker Knowledge Session 1 recording
============================================

.. raw:: html

	<p><a data-card-appearance="inline" href="https://drive.google.com/file/d/1A1hLS0ZUBT6isf-TtcRI8i2OZzfhe3UI/view?usp=sharing">https://drive.google.com/file/d/1A1hLS0ZUBT6isf-TtcRI8i2OZzfhe3UI/view?usp=sharing</a> </p>


Learn Dockers
=============

.. raw:: html

	<p><a href="https://everlytics.atlassian.net/wiki/spaces/TC/pages/862387815/Udemy+Courses#DevOps">https://everlytics.atlassian.net/wiki/spaces/TC/pages/862387815/Udemy+Courses#DevOps</a> </p>

**********************
Common Docker Commands
**********************


Containers
==========

.. code-block:: text

	docker container ls                 --list the containers
	docker exec -it kib01 bash          --get inside a container shell
	docker container inspect kib01      --ports, network, env, passwords etc.
	docker container top kib01          --list the processes running in the container
	docker container stats kib01        --cpu%, mem, net traffic, PIDs (ctrl-c to exit)
	docker container logs -f kib01      --see live logs (ctrl-c to exit)
	docker container stop $(docker container ls -a -q)  --stop all containers
	docker container rm $(docker container ls -a -q)  --remove all containers
	docker stats -a                     --see resource consumption stats



5 ways to debug an exploding Docker container
---------------------------------------------

.. code-block:: text

	docker commit <container_id> my-broken-container &&
	docker run -it my-broken-container /bin/bash
	
	#If nginx container is not starting, use below cmd to find the reason
	docker run -it --network es_elastic --name nginx_test -p 443:12361 nginx:latest



Images
======

.. code-block:: text

	docker pull registry/image:tag      --pull the image from registry
	docker image build -t kib01 .       --builds image from Dockerfile present in dir
	docker image ls                     --list the images
	docker image history 185cc3502662   --show the layers of the image
	docker image prune -f               --to clean up just "dangling" images
	docker rmi -f $(docker images -f dangling=true -q)  --to clean up just "dangling" images (other way)
	
	docker volume ls -q | xargs docker volume rm  --delete all docker volumes



Network
=======

.. code-block:: bash

	docker network ls                   --list the docker (virtual) networks
	docker network inspect bridge       --network details



Volumes
=======

.. code-block:: bash

	docker volume ls                    --list the persistent volumes
	docker volume inspect es_data01     --displays volume location on host



Registry
========

.. code-block:: bash

	docker container stop registry && docker container rm -v registry   #stop registry and remove all data
	curl -X GET http://registry.4pointx.com:5000/v2/_catalog   #list the repos in the registry



Registry Cleanup
----------------

.. code-block:: text

	curl -v --silent -H "Accept: application/vnd.docker.distribution.manifest.v2+json" -X GET http://localhost:5000/v2/4pointx/4px_kib/manifests/build 2>&1 | grep Docker-Content-Digest | awk '{print ($3)}'
	curl -v -X DELETE http://localhost:5000/v2/4pointx/4px_kib/manifests/<ID_from_above_cmd>
	docker exec registry bin/registry garbage-collect --delete-untagged /etc/docker/registry/config.yml
	docker restart registry



Space Recovery
==============

.. code-block:: text

	docker image prune    //removes dangling images
	docker system prune   //removes dangling/ unused everything (dangling images, build broken images, unused volumes etc)
	


.. toctree::
