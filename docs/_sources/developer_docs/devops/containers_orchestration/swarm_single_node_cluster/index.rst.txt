#########################
Swarm single node cluster
#########################


************
introduction
************

.. raw:: html

	<p>This design document describes the architecture single node Swarm clusters that would be engaged in the deployment of modules and services in the later releases.</p>

*********
Services:
*********

.. raw:: html

	<ol start="1"><li><p>OSD: OpensearchDashboards</p></li><li><p>OS: Opensearch</p></li><li><p>Config</p></li><li><p>EMS</p></li><li><p>PDM</p></li><li><p>CM</p></li><li><p>alerting</p></li><li><p>UM: usermanagement</p></li><li><p>scheduler</p></li><li><p>Jupyternotebooks</p></li><li><p>ML</p></li><li><p>kafka broker and zookeeper</p></li><li><p>kafka consumer</p></li><li><p>radpanda(UI for kafka)</p></li><li><p>nginx</p></li><li><p>monitoring tools:</p><ol start="1"><li><p>grafana</p></li><li><p>prometheus</p></li><li><p>cadvisor</p></li><li><p>node-exporter</p></li><li><p>portainer</p></li></ol></li></ol>

.. toctree::
