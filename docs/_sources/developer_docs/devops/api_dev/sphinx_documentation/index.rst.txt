####################
Sphinx Documentation
####################


.. raw:: html

	<p><strong>Step 1 Creating Python script along with wordstring.</strong><br/><br/>Sphinx-Confluence Builder is installed in Dev Env. Go to the Sphinx directory, create a python script.</p>

.. raw:: html

	<p><br/>path =&gt;/home/4px/sphinx-demo<br/><br/>Below Docstring needs to be added in at starting of a python script. The Docstring will get converted into the documentation of the confluence.<br/>Ex-<ac:link><ri:page ri:content-title="Plants API" ri:version-at-save="36"></ri:page><ac:link-body>Plants Api's Documentation page in confluence</ac:link-body></ac:link><br/><br/>The plants.py script along with doctstring is present in gitlab.<br/>Ex-<a data-card-appearance="inline" href="https://gitlab.com/everlytics/4px.config/-/blob/build/apis/plants.py">https://gitlab.com/everlytics/4px.config/-/blob/build/apis/plants.py</a> <br/><strong><span style="color: rgb(255,196,0);">Note- Pls don’t make changes in the indentation of the docstring to avoid any errors. Keep the indentation as per below doctstring and update docstring accordingly.</span></strong></p>

.. code-block:: text

	"""
	
	**Create Plant**
	===================
	    Create a new plant in an existing site.
	
	      **ENDPOINT**::
	
	         POST config/plants
	      **PARAMETERS**::
	
	         {
	         "plant_name" : "New Plant",
	         "site_id" : "228c0003-abc0-4806-860f-de3ff7d98ae5",
	         "shops" : "blr,abc"
	         }
	
	      **RETURNS**::
	
	        {
	        "plant_name": "New Plant",
	        "site_id": "228c0003-abc0-4806-860f-de3ff7d98ae5",
	        "plant_id":"97dfd449-53f5-4fd0-88f8-cf903dc4bd5b"
	        }
	
	      **ERRORS**::
	
	        {
	        "error" : "site not found"
	        }
	      Example: site not found
	
	
	      **EXAMPLE**::
	
	        curl -X POST https://api.4pointx.com/config/plants \
	        --data "{\"plant_name\" : \"Demo Plant\",\"site_id\" : \"228c0003-abc0-4806-860f-de3ff7d98ae5\",\"shops\" : \"blr,abc\"}"
	
	
	**Delete Plant**
	=====================
	    Delete a plant from the site.
	
	      **ENDPOINT**::
	
	         DELETE config/plants
	      **PARAMETERS**::
	
	         {
	         "id":"78cb9b57-024a-44d9-b185-4da40017fcf8"
	         }
	      **RETURNS**::
	
	         "Asset exist for 78cb9b57-024a-44d9-b185-4da40017fcf8 "
	         or
	         "Events exist for 78cb9b57-024a-44d9-b185-4da40017fcf8"
	         or
	         "Pipeline exist for 78cb9b57-024a-44d9-b185-4da40017fcf8"
	      **ERRORS**::
	
	         "Invalid Plant for 78cb9b57-024a-44d9-b185-4da40017fcf8"
	      Example: Plant not found in site.
	
	      **EXAMPLE**::
	
	         curl -X DELETE https://api.4pointx.com/config/plants \
	         --data "{\"id\" : \"78cb9b57-024a-44d9-b185-4da40017fcf8\"}"
	
	**Update Plant**
	==================
	    Update a plant from the site.
	
	     **ENDPOINT**::
	
	       PUT config/plants
	     **PARAMETERS**::
	
	        {
	        "plant_name"="Plant",
	        "site"="JSW",
	        "shops"="abc,xyz"
	        }
	     **RETURNS**::
	
	        "{"plant_id": "97dfd449-53f5-4fd0-88f8-cf903dc4bd5b",
	        "plant_name":"Plant",
	        "site":"JSW",
	        "site_id": "228c0003-abc0-4806-860f-de3ff7d98ae5"}"
	        or
	        "No changes to update"
	     **ERRORS**::
	
	        "Invalid plant for id "97dfd449-53f5-4fd0-88f8-cf903dc4bd5b"
	     Example: Plant not found in site.
	
	     **EXAMPLE**::
	
	        curl -X PUT https://api.4pointx.com/config/plants \
	        --data "{\"plant_name\" : \"Plant\",\"site_id\" : \"228c0003-abc0-4806-860f-de3ff7d98ae5\",\"shops\" : \"abc,xyz\"}"
	
	
	
	**Fetch Plant**
	==================
	    Fetch a plant from the site.
	
	     **ENDPOINT**::
	
	       GET config/plants
	     **PARAMETERS**::
	
	        {
	        "id":"97dfd449-53f5-4fd0-88f8-cf903dc4bd5b"
	        }
	     **RETURNS**::
	
	        {
	        "plant_id":"97dfd449-53f5-4fd0-88f8-cf903dc4bd5b",
	        "plant_name":"Plant",
	        "site":"JSW",
	        "site_id": "228c0003-abc0-4806-860f-de3ff7d98ae5"
	        }
	     **ERRORS**::
	
	        "Invalid plant for id "97dfd449-53f5-4fd0-88f8-cf903dc4bd5b"
	     Example: Plant not found in site.
	
	     **EXAMPLE**::
	
	        curl -X GET https://api.4pointx.com/config/plants \
	        --data "{\"id\" : \"97dfd449-53f5-4fd0-88f8-cf903dc4bd5b\"}"
	
	
	**Fetch List of Plants**
	==========================
	    Fetch list of plants from the site.
	
	     **ENDPOINT**::
	
	        GET config/plants
	     **PARAMETERS**::
	
	        {
	        "site_id"="228c0003-abc0-4806-860f-de3ff7d98ae5"
	        }
	     **RETURNS**::
	
	        {
	        ["c053dbf6-cdc2-400a-8e09-4e5052e329bd","Plant 2","JSW","228c0003-abc0-4806-860f-de3ff7d98ae5",2,2],
	        ["d86739e1-410c-4d85-9b54-cddf4b330c75","Demo3","JSW","228c0003-abc0-4806-860f-de3ff7d98ae5",2,0],
	        ["97dfd449-53f5-4fd0-88f8-cf903dc4bd5b","Plant","JSW","228c0003-abc0-4806-860f-de3ff7d98ae5",2,0]
	        }
	     **ERRORS**::
	
	        {
	        Invalid plant for site id 228c0003-abc0-4806-860f-de3ff7d98ae5
	        }
	     Example: Invalid site id for plants.
	
	     **EXAMPLE**::
	
	        curl -X GET https://api.4pointx.com/config/plants \
	        --data "{\"site_id\" : \"228c0003-abc0-4806-860f-de3ff7d98ae5\"}"
	
	"""                                       



Step 2- Adding script name to index.rst and creating <Api_name.rst> file.
=========================================================================

.. raw:: html

	<p>Enter docs from sphinx and then enter source from docs</p>

.. raw:: html

	<p><strong>Content for the Api_Name.rst file</strong>:<br/>path=&gt;/home/4px/sphinx-demo/docs/source</p>

.. code-block:: text

	 .. spectRRa documentation master file, created by
	 sphinx-quickstart on Fri Apr 24 11:55:57 2020.
	 You can adapt this file completely to your liking, but it should at least
	 contain the root `toctree` directive.
	
	 .. toctree::
	       :maxdepth: 2
	       :caption: Contents:
	
	**Api Name**
	=================
	
	.. automodule:: Api Name
	    :members:


.. raw:: html

	<p><br/><strong>Adding python script name to index.rst-</strong><br/>index.rst-This file will index the Apis documentation in the API Docs page in confluence.<br/>path =&gt;/home/4px/sphinx-demo/docs/source</p>

.. code-block:: text

	.. spectRRa documentation master file, created by
	   sphinx-quickstart on Fri Apr 24 11:55:57 2020.
	   You can adapt this file completely to your liking, but it should at least
	   contain the root `toctree` directive.
	
	**API Docs**
	***************
	.. toctree::
	   Api_Name
	   assets
	   plants
	   pipelines
	   sites


.. raw:: html

	<p><strong><span style="color: rgb(255,196,0);">Note-Pls don’t make changes in indentation otherwise, documentation in confluence will not be updated.</span></strong><br/></p>


Step 3-Build Documentation in Confluence-
=========================================

.. raw:: html

	<p>Go to cd /sphinx-demo/docs directory.</p>

.. raw:: html

	<p>Run Command-This command will make documentation in confluence.<br/></p>

.. code-block:: text

	make confluence


.. toctree::
