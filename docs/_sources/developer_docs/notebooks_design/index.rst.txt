################
Notebooks design
################


.. raw:: html

	<p>The Notebooks module (formerly called ‘Data Science Workbench’) is a plug-in within the 4PointX IIoT Platform and enables users to write Python code in order to analyse the data or create custom solutions on top of the data stored in the platform. This is done through the creation of customisable IPython notebooks that can connect to the underlying Elasticsearch data store. The workbench also allows users to schedule these notebooks to be run at regular intervals and also to monitor them on an ongoing basis. </p>



.. raw:: html

	<p>As of Version 0.5 the workbench consists of three primary components:</p>

.. raw:: html

	<ul><li><p>Embedded Jupyterhub - for creating IPython notebooks</p></li><li><p>Frontend UI - for scheduling and monitoring the notebooks</p></li><li><p>ML Automation Framework -  a backend process that manages the schedule queue for  scheduling these notebooks</p></li></ul>

***********
Jupyterhub 
***********

.. raw:: html

	<p>Jupyterhub is the best way to serve Jupyter notebooks for multiple users. It is a multi-user Hub that spawns, manages, and proxies multiple instances of the single-user Jupyter notebook server. </p>

.. raw:: html

	<p>Four subsystems make up JupyterHub:</p>

.. raw:: html

	<ul><li><p>a Hub (tornado process) that is the heart of JupyterHub</p></li><li><p>a configurable http proxy (node-http-proxy) that receives the requests from the client’s browser</p></li><li><p>multiple single-user Jupyter notebook servers (Python/IPython/tornado) that are monitored by Spawners</p></li><li><p>an authentication class that manages how users can access the system</p></li></ul>

.. raw:: html

	<img src='/docs/_static/media/fUX-_JXxwcZGgMeCjeNPFFP16QB9m0NMuRU76UJ4MpOkB95lqMDG3njAZXkM0oz_0Nc3MBHpCFSLJmddbAsQwe2xP8tzenTAidfI1p4gpsedecrtIydRNO-sMAvjzlEMuaG0vSSD'>


Embedded Jupyterhub 
====================

.. raw:: html

	<p>In order to make the DSW possible, we have deployed a single-user instance of Jupyterhub and embedded the same into our platform. The details of the deployment are as follows:</p>

.. raw:: html

	<ul><li><p>Host: <code>192.168.2.9</code></p></li><li><p>Port: <code>8000</code> (default)</p></li><li><p>Username: andrea.p</p></li><li><p>Linux Password: <code>Ch@ngeme415</code></p></li></ul>


Installation 
=============

.. raw:: html

	<p>The steps followed for installing and configuring this Jupyterhub installation are as follows:</p>

.. raw:: html

	<ol><li><p>Enable Python3 [for CentOS]</p></li></ol>

.. raw:: html

	<p><code>sudo yum install centos-release-scl</code></p>

.. raw:: html

	<p><code>sudo yum install rh-python36</code></p>

.. raw:: html

	<p><code>scl enable rh-python36 bash</code></p>

.. raw:: html

	<ol><li><p><strong>Verify Python3 installation</strong>, should return ‘Python 3.x’</p></li></ol>

.. raw:: html

	<p><code>python --version</code></p>

.. raw:: html

	<ol><li><p><strong>Install Jupyterhub and Configurable HTTP Proxy</strong>. Jupyterhub git is used to bypass a known issue with the installation files available on <a href="http://pypi.org">pypi.org</a>. <strong>Dependency:</strong> HTTP Proxy requires NPM to be installed globally. </p></li></ol>

.. raw:: html

	<p><code>pip install --user git+https://github.com/jupyterhub/jupyterhub.git@master</code></p>

.. raw:: html

	<p><code>npm install -g configurable-http-proxy</code></p>

.. raw:: html

	<p><code>python3 -m pip --user install notebook</code></p>

.. raw:: html

	<ol><li><p><strong>Verify installation</strong></p><ul><li><p>if both commands return a set of instructions for using each respectively, installation is bug free.</p></li></ul></li></ol>

.. raw:: html

	<p><code>jupyterhub -h </code></p>

.. raw:: html

	<p><code>configurable-http-proxy -h</code></p>

.. raw:: html

	<ol><li><p><strong>Generate a config file</strong> for Jupyterhub</p></li></ol>

.. raw:: html

	<p><code>jupyterhub --generate-config</code></p>

.. raw:: html

	<ol><li><p><strong>To start</strong> in single user mode with no proxy</p></li></ol>

.. raw:: html

	<p><code>nohup jupyterhub &gt; jupyterhub_log.log &amp; </code></p>

.. raw:: html

	<ol><li><p><strong>Edit config file <em>jupyterhub_config.py</em>:</strong></p></li></ol>

.. raw:: html

	<p><code>c.Spawner.args = [‘--NotebookApp.tornado_settings={“headers”:{“Content-Security-Policy”: “frame-ancestors * self host_ip:port_no”}}’]</code></p>

.. raw:: html

	<p><code>c.JupyterHub.tornado_settings = { ‘headers’: { ‘Content-Security-Policy’: “frame-ancestors * self host_ip:port_no”} </code></p>

.. raw:: html

	<p><code>c.NotebookApp.tornado_settings = { ‘headers’ : { ‘Content-Security-Policy’ : “frame-ancestors * self host_ip:port_no”}}</code></p>

.. raw:: html

	<p><code>#REPLACE host_ip:port_no WITH IP, PORT OR URL AS REQUIRED#</code></p>

*****************
Jupyterhub Docker
*****************

.. raw:: html

	<p>A dockerised image of Jupyterhub can be downloaded from <u><a href="https://hub.docker.com/r/jupyterhub/jupyterhub/">here</a></u> using the command: </p>

.. raw:: html

	<p><code>docker pull jupyterhub/jupyterhub</code></p>

.. raw:: html

	<p>The image only contains the core Jupyterhub service itself and <strong>does not</strong> contain other components like the Notebook installation that has to be manually installed using: </p>

.. raw:: html

	<p><code>python3 -m pip --user install notebook</code></p>

.. raw:: html

	<p>The image also does not contain a config for the hub service, the config can be generated using:  </p>

.. raw:: html

	<p><code>jupyterhub --generate-config</code></p>

.. raw:: html

	<p>Once created, this can be edited to reflect necessary changes as above. (Refer step 7 <u><a href="https://docs.google.com/document/d/1u0NPuKmzTPHfJzfyFaReU21DKP3za233Q0GROFEAiQs/edit#heading=h.9868ovigkdmi">here</a></u>) </p>

.. raw:: html

	<p>The hub image can be started using:</p>

.. raw:: html

	<p><code>docker run -d -p 8000:8000 --name jupyterhub jupyterhub/jupyterhub jupyterhub</code></p>

.. raw:: html

	<p>This command will create a container named <code>jupyterhub </code>that you can stop and resume with docker stop/start. If you want to run docker on a computer that has a public IP then you should (as in MUST) secure it with ssl by adding ssl options to your docker configuration or using a ssl enabled proxy.</p>

.. raw:: html

	<p>In order to spawn a root shell within the docker container use:</p>

.. raw:: html

	<p><code>docker exec -it jupyterhub bash</code></p>

.. raw:: html

	<p>The root shell can be used to create system users in the container i.e. the accounts to be used for authentication in Jupyterhub’s default configuration.</p>

***********************
ML Automation Framework
***********************

.. raw:: html

	<p>This is a Flask API based framework that is used to dynamically schedule any scripts created through the workbench. As this a common underlying framework used by most solutions within the platform, it has been documented separately. Click <u><a href="https://docs.google.com/document/d/1A6NPoYPhhy1LCCLXnRrOCWc2xwAx2XNqKKLLGBEHStM/edit?usp=sharing">here</a></u> for the complete documentation. </p>

.. raw:: html

	<p>As of version 0.5 of the DSW, scripts have to be manually scheduled by implementing the following steps:</p>

.. raw:: html

	<ol><li><p>Download the IPython notebook as Python script. </p></li><li><p>Rename the Python file as per the following conventions: (solution name can be anything provided by the user) </p><ol><li><p>For build codes - <code>custom_&lt;solution_name&gt;_build.py</code></p></li><li><p>For score codes - <code>custom_&lt;solution_name&gt;_score.py</code></p></li></ol></li><li><p>Move the Python script to the correct folder on the VM where the Automation Framework is currently deployed, as per the following rules:</p><ol><li><p>Build scripts: <code>/cyberonix/build_code/</code></p></li><li><p>Score scripts: <code>/cyberonix/score_code/</code></p></li><li><p>Config files: <code>/cyberonix/config/</code></p></li></ol></li><li><p>Update the following indices in Elasticsearch as required:</p><ol><li><p>standard_users_company: update with customer name if not ‘demo’</p></li><li><p>standard_users_asset: update with customer id from (a) and asset name if not already present </p></li><li><p>standard_users_asset_app: update with id from (b) and app name as ‘custom’ if not already present </p></li><li><p>standard_users_asset_app_solution: update with app name from (c), solution name (as provided by user), solution type as ‘ml’, score frequency (in minutes) and build frequency (in days). </p></li></ol></li></ol>

************
Front-end UI
************

.. raw:: html

	<p>The user interface for DSW is as a plug-in within 4PointX and consists of three tabs:</p>

.. raw:: html

	<ul><li><p>Notebook - where embedded Jupyterhub is present </p></li><li><p>Scheduler - where scheduled and unscheduled scripts can be monitored and scheduled</p></li></ul>

.. raw:: html

	<img src='/docs/_static/media/image-20200521-121734.png'>

.. raw:: html

	<p><br/></p>

.. raw:: html

	<p>As of version 1.0,  the ‘Scheduler’ tab is completely functional, allowing users to schedule, re-schedule and stop schedules for various scripts and reflecting the same both in the table and in the metrics displayed above. This is made possible by the use of the following Python scripts found <u><a href="https://gitlab.com/everlytics/diya.dsw/-/tree/master/queries">here</a></u> and explored in detail below:</p>

.. raw:: html

	<ul><li><p><code>dsw_scheduler_get_metrics.py</code></p></li><li><p><code>dsw_scheduler_get_schedule.py</code></p></li><li><p><code>dsw_scheduler_get_table.py</code></p></li><li><p><code>dsw_scheduler_set_schedule.py</code></p></li><li><p><code>dsw_scheduler_set_stopschedule.py</code></p></li></ul>


dsw_scheduler_get_metrics.py
============================

.. raw:: html

	<ul><li><p>Is executed when the ‘Scheduler’ tab is opened</p></li><li><p>Connects to the VM where Jupyterhub is deployed and reads the subdirectory in which notebooks are created - ‘all_notebooks’</p></li><li><p>Retrieves the list of files with a ‘.ipynb’ extension and the time when they were last modified using the ‘stat’ command</p></li><li><p>Connects to Elasticsearch and retrieves a list of scheduled scripts from the standard* index. </p></li><li><p>Returns counts of:</p><ul><li><p>Total scripts</p></li><li><p>Scheduled scripts</p></li><li><p>Unscheduled scripts</p></li></ul></li></ul>


dsw_scheduler_get_schedule.py
=============================

.. raw:: html

	<ul><li><p>Is executed when the the edit or start schedule buttons are used in the ‘Scheduler’ tab table</p></li><li><p>Takes the name of script selected as input and retrieves the schedule as stored in the standard* index, if not present displays default as “30 Days”</p></li></ul>


dsw_scheduler_get_table.py
==========================

.. raw:: html

	<ul><li><p>Is executed when the ‘Scheduler’ tab is opened</p></li><li><p>Connects to the VM where Jupyterhub is deployed and reads the subdirectory in which notebooks are created ‘all_notebooks’ </p></li><li><p>Retrieves the list of files with a ‘.ipynb’ extension</p></li><li><p>Connects to the Elasticsearch and retrieves the list of scheduled scripts from the standard* index</p></li><li><p>Collates data from both sources to display the list of scripts both scheduled and unscheduled along with relevant data i.e. schedule, script creator, number of runs, date last run and current status. </p></li></ul>


dsw_scheduler_set_schedule.py
=============================

.. raw:: html

	<ul><li><p>Is executed when the edit / start schedule dialogue box is saved in the “Scheduler” tab </p></li><li><p>Inputs are, the name of the script selected and the schedule entered by the user</p></li><li><p>Connects to the standard* index on Elasticsearch and:</p><ul><li><p>updates the schedule if the script has been previously scheduled and status to “Scheduled”</p></li><li><p>creates a new record if the script has not been scheduled before and status to “Scheduled”</p></li></ul></li></ul>


dsw_scheduler_set_stopschedule.py
=================================

.. raw:: html

	<ul><li><p>Is executed when the stop schedule dialogue box is saved in the ‘Scheduler’ tab </p></li><li><p>Input is the name of the script selected </p></li><li><p>Connects to the standard* index on Elasticsearch and updates the status of the script to “Not Scheduled”</p></li></ul>



**********
Jupyterhub
**********

.. raw:: html

	<p>The embedded Jupyterhub installation does not work without the use of an extension called ‘Ignore X-Frame Headers’ that can be installed from <u><a href="https://chrome.google.com/webstore/detail/ignore-x-frame-headers/gleekbfjekiniecknbkamfmkohkpodhe">here</a></u>. This is due to an issue with the Content Security Policy of iframes. This issue has been bypassed to a large extent by modifying the configuration file of Jupyterhub as detailed above however, the modifications have not been able to bypass the CSP on the spawner i.e. the component of JH that is responsible for spawning new notebook servers and new notebooks. As such, while it is possible to log in to JH and view the directory tree where the notebooks are present it is NOT possible to start the user server or edit / create notebooks without use of the extension. </p>

***********************
ML Automation Framework
***********************

.. raw:: html

	<p>The framework has been update to schedule non-ML tasks i.e. those that don’t have the keywords ‘build’ or ‘score’ scripts. However there is a caveat as the standard* index structure itself has not been changed, as a result, the schedule of “custom” scripts i.e. scripts created in DSW is stored in a field known as ‘score_frequency’ that only works if the unit is in minutes; resulting in a less than friendly UI experience. Future versions of the framework will be modified to change the way the schedule of a given script is stored. </p>



.. raw:: html

	<ol><li><p><u><a href="https://jupyterhub.readthedocs.io/en/stable/index.html">Jupyterhub documentation</a></u></p></li><li><p><a href="https://hub.docker.com/r/jupyterhub/jupyterhub/">Jupyterhub docker documentation</a></p></li><li><p><u><a href="https://docs.google.com/document/d/1A6NPoYPhhy1LCCLXnRrOCWc2xwAx2XNqKKLLGBEHStM/edit?usp=sharing">ML Automation Framework documentation</a></u></p></li></ol>

.. toctree::

	adding_csp_rules_to_kibana/index