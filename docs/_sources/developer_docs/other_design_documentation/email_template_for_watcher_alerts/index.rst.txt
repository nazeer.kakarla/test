#################################
Email template for Watcher Alerts
#################################


.. raw:: html

	<p> &lt;from&gt; :  <a href="mailto:test.alert@4pointx.com">test.alert@4pointx.com</a></p>

.. raw:: html

	<p>&lt;to&gt;:   <a href="mailto:XXXXX@gmail.com">XXXXX@gmail.com</a></p>

.. raw:: html

	<p>&lt;subject&gt;:  {{<a href="http://ctx.metadata.name">ctx.metadata.name</a>}} “ Watcher Notification Alert "</p>

.. raw:: html

	<p>Dear user,</p>

.. raw:: html

	<p>{{ctx.aggregations.metricAgg.value}} has exceeded the set limit {{ctx.params.threshold}}</p>

.. raw:: html

	<p>Start Time: {{ctx.trigger_event.triggered_time}}</p>

.. raw:: html

	<p>Number of hits: {{ctx.payload.hits.total}}</p>

.. raw:: html

	<p>Duration: {{ctx.result.execution_duration}}</p>

.. raw:: html

	<p>For details, please visit the 4PointX system: </p>

.. raw:: html

	<p><a href="https://demo.4pointx.com">https://demo.4pointx.com</a></p>

.. raw:: html

	<p><br/></p>

.. toctree::
