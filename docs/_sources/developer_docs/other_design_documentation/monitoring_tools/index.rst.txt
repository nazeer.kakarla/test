################
Monitoring Tools
################




.. raw:: html

	<p>Docker monitoring and control. Users: DevOps</p>

.. raw:: html

	<p><a href="https://test.com:9443/">https://test.4pointx.com:9443</a></p>

.. raw:: html

	<p>uname: admin<br/>pwd: qJfFHgcHyHNnVU1r0YK8MfNVedyVnGu8</p>

.. raw:: html

	<p>License Key (Business Edition, 5-node): Not sure</p>

.. raw:: html

	<p><code>2-2Zb0rjC6Bl0YEiRQm1prZG8mTn6Jr3B8jXODgCxF17KOOnxyosoghbdv7jTcelbRh4BimC7yQArPj+M+esU=</code></p>

*******
Grafana
*******

.. raw:: html

	<p>Host system resources monitoring. Uses Prometheus. </p>

.. raw:: html

	<p>Data comes from:</p>

.. raw:: html

	<ol start="1"><li><p>node-exporter (container usage data) and </p></li><li><p>cadvisor (vm resources usage)</p></li></ol>

.. raw:: html

	<p><a href="http://test.4pointx.com:3000">http://test.4pointx.com:3000</a></p>

.. raw:: html

	<p>uname: admin</p>

.. raw:: html

	<p>pwd: 4px_admin_329</p>

.. raw:: html

	<p>Pwd hard coded in <a href="https://gitlab.com/everlytics/4px.setup/-/blame/test/dockerfiles/monitoring_tools/docker-stack.yml#L114">https://gitlab.com/everlytics/4px.setup/-/blame/test/dockerfiles/monitoring_tools/docker-stack.yml#L114</a></p>

**************************
Redpanda (previously Kowl)
**************************

.. raw:: html

	<p>For monitoring Kafka Topics and Kafka Consumers</p>

.. raw:: html

	<p><a href="http://test.4pointx.com:8080/">http://test.4pointx.com:8080/</a></p>

.. raw:: html

	<p>Known issue: not accessible from outside the VM using the DNS name or public IP.</p>

.. raw:: html

	<p>Temporary Workaround: Configure port forwarding in your laptop using the following cmd:</p>

.. code-block:: text

	ssh -L 8080:0.0.0.0:8080 -i 4px@test-swarm.pem 4px@test.4pointx.com


.. raw:: html

	<p>Once done, you can access it from browser using address: <a href="http://localhost:8080/overview">http://localhost:8080</a></p>

.. toctree::

	observability_types/index