###################
Observability Types
###################


.. raw:: html

	<p><ac:link><ri:user ri:account-id="605c239d630024006fceaa08"></ri:user></ac:link> We can use this page to document the design thoughts. Capture the metrics, viz and alerts (with threshold conditions).</p>

****************************
1. Infra (VM) Observability:
****************************

.. raw:: html

	<p>We must use Cloud provider Monitoring Tools to Monitor INFRA Level Parameters.<br/>For On premesis intallation setup, Client can provide us their 3rd Party Infra Monitoring Tool such as Zabbix for TCL.</p>

.. raw:: html

	<blockquote><p> Can we continue to use our current Grafana/cadvisor/node-exporter for K8S setup also?<br/><br/>For INFRA LEVEL Monitoring using Grafana is possible, but it's a cumbersome process with a lot of dependencies. The best way is to manage at Cloud Level or 3rd Party Level (Zabbix).<br/><br/>1. Need to have DB/(Time Series DB) such as Prometheus to store metrics of INFRA.<br/>2. We need an exporter(MySQL exporter/node exporter) process in each VM that will export data to DB.<br/>3. Need to have one Dashboarding and Alerting Tool (Grafana) along with SMTP Server access to send alerts over mails.<br/>4. Storing Data and processing for INFRA Monitoring will again part of K8S Cluster will need lot of development in VIZ , Dashboards, Alert Configuring and managing this Stack.</p></blockquote>

.. raw:: html

	<p><ac:link><ri:user ri:account-id="5e635bc1ca52800b50582bb8"></ri:user></ac:link> , Pls let us know, how are you planning to Manage INFRA Level Alerts?</p>


Parameters to Monitor at INFRA Level (VM): (Both Alerts & Dashboards)
=====================================================================


Alerts Thresholds for Last 15 mins:
-----------------------------------

.. list-table::
	:header-rows: 1

	* - Parameter
	  - Thersholds
	  - Severity
	* - CPU Utilzation
	  - CPU > 80%CPU > 90%CPU > 95%
	  - WarningHighCritical
	* - RAM Utilization
	  - RAM > 80%RAM > 90%RAM > 95%
	  - WarningHighCritical
	* - Disk Storage Utilization
	  - Disk Storage > 80%Disk Storage > 90%Disk Storage > 95%
	  - WarningHighCritical
	* - VM Down
	  - VM Down
	  - Critical
	* - Network OutageNetwork LatencyICMP Ping Failure (Packet Drops)ICMP Ping Failure (N/w Down)
	  - 1. N/W Latency > 200 ms2. Ping Response packet drops >10%3.ICMP Ping Failure
	  - 1. Warning2. High3. Critical


******************************************
2. K8S Cluster Observability & Management:
******************************************

.. raw:: html

	<ol start="1"><li><p>In Order to Monitor the live K8S Cluster components, currently we are using <a href="https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/">K8S Dashboards</a> . <br/>Its opensource application for realtime Monitoring and Managing K8S Cluster components,designed by K8S. We can scale up/down,delete, edit K8S Components such as Deployement,Services, Persistant Volumes using K8S Dashboards GUI without logging into K8S Master VM.<br/><br/>Advantages:<br/>1. Its very handy tool in Management of K8S Cluster for real time monitoring including logs of each K8S components.<br/><br/>Drawbacks:<br/>1. Historical data related to K8S Cluster is not stored. (<ac:link><ri:user ri:account-id="712020:fa09b262-1f13-4bc8-9475-89772ceb5c17"></ri:user></ac:link> Pls check and confirm)<br/>2. If any K8S Cluster Component such as pod is not up or in pending state, or went down, real time alerts are not possible. <br/><br/>2nd Drawback can be resolved by having replica sets, which will make sure certain number of pods in deployment will be always up, if any pod went down, or its state changed from Running to any other state, K8S will immediately spin up new pod and kill the Non Running Pod by itself.<br/>(<ac:link><ri:user ri:account-id="712020:fa09b262-1f13-4bc8-9475-89772ceb5c17"></ri:user></ac:link> Pls confirm if my understanding is correct w.r.t 2nd Drawback and its soultion).</p></li></ol>

***********************************
3. Application Level Observability:
***********************************

.. raw:: html

	<p><ac:link><ri:user ri:account-id="5e635bc1ca52800b50582bb8"></ri:user></ac:link> Comments:</p>

.. raw:: html

	<blockquote><p>For application monitoring we currently have logging enabled (for API/Python/non-API scripts), and they are being ingested to system-logs index. This is fine for now.</p></blockquote>

.. raw:: html

	<p>For Application Level Observality (Alerting &amp; Dashboards) for Opensearch DB, API’s, Kafka, we can use custom Dashboards &amp; Alerting application such as Grafana or OSD itself.</p>

.. raw:: html

	<p>3.1 OpeneSearch DataBase Monitoring:</p>

.. raw:: html

	<p><ac:link><ri:user ri:account-id="712020:fa09b262-1f13-4bc8-9475-89772ceb5c17"></ri:user></ac:link> ,Pls check if Opensearch provide any monitoring feature in built to monitor OpenSearch DB.</p>

.. raw:: html

	<p>3.2 Kafka Application: We are using Red panda monitoring application in our K8S Cluster.</p>

.. raw:: html

	<p>3.3 JupyterHUB &amp; Nginx: </p>

.. raw:: html

	<p><ac:link><ri:user ri:account-id="712020:fa09b262-1f13-4bc8-9475-89772ceb5c17"></ri:user></ac:link> , Pls check if we can monitor (Dashboards &amp; Alerts) Nginx application using Grafana or OSD (opensearch Dashboards)</p>

.. raw:: html

	<p><br/><ac:link><ri:user ri:account-id="712020:fa09b262-1f13-4bc8-9475-89772ceb5c17"></ri:user></ac:link> Pls update this doc with link with your findings. <br/>Pls Log time in Jira Task: <ac:structured-macro ac:macro-id="bf977163-0001-4f33-ac25-ef393e2280fc" ac:name="jira" ac:schema-version="1"><ac:parameter ac:name="key">FPX-1773</ac:parameter><ac:parameter ac:name="serverId">610de044-3866-39dc-b8d8-59f995572ede</ac:parameter><ac:parameter ac:name="server">System JIRA</ac:parameter></ac:structured-macro> </p>

.. toctree::
