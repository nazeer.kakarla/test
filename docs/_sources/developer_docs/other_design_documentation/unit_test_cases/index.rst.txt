###############
Unit-Test Cases
###############




****************
1. Pipelines Tab
****************

.. raw:: html

	<p>TODO : Separate units w.r.t APIs</p>

.. raw:: html

	<p>Below are the test cases for the pipeline code.</p>

.. list-table::
	:header-rows: 1

	* - ID
	  - Test Case
	  - Description
	  - Input
	  - Expected Output
	* - 1
	  - makeConnection():
	  - Creates a secure connection to Elasticsearch and return the Client Object
	  - ES host, port, Auth Parameters
	  - Elasticsearch Client
	* - 2
	  - change_date_format():
	  - Changes the format of unix milli second level timestamp to a Human readable one
	  - epoch timestamp in milliseconds
	  - Formatted Timestamp
	* - 3
	  - makeSSH():
	  - SSH into the Server
	  - PEM key, host, username
	  - SSH Client
	* - 4
	  - makeSSHProd():
	  - SSH into the Producer Server
	  - PEM key, host, username
	  - SSH Client
	* - 5
	  - makeSSHCons():
	  - SSH into the Consumer Server
	  - PEM key, host, username
	  - SSH Client
	* - 6
	  - get_timestamp_data():
	  - Retrieves the last 30 days document count for all the indices that satisfies the pattern demo*
	  - API Request
	  - HTTP 200 status response
	* - 7
	  - getdata():
	  - Total no. of pipelines, Online pipeline count, Total no. of events for last 30 days. Index pattern : demo*
	  - API Request
	  - HTTP 200 status response
	* - 8
	  - get_event_count():
	  - Takes the pipeline name as input and returns the count of documents for the given pipeline
	  - pipeline_name
	  - Count of documents
	* - 9
	  - get_last_event():
	  - Takes the pipeline name as input and returns the timestamp of the last record for the given pipeline
	  - pipeline_name
	  - Last eventTimeStamp
	* - 10
	  - pipeline_exists_iotgateway():
	  - Function to check whether the pipeline is already exists or not in the system pipelines index
	  - pipeline_name
	  - True (If exists) / False (If not)
	* - 11
	  - fetch_pipeline_table():
	  - Retrieves all the documents from the system pipelines index and masks the unnecessary fields.
	  - API Request
	  - HTTP 200 status response
	* - 12
	  - fetch_pipeline_table(index):
	  - Retrieves all the documents from the system pipelines index
	  - system pipelines index name
	  - String representation of a JSON array
	* - 13
	  - start_publishing_delimitedfile():
	  - SSH into the Server & Invokes the Kafka Steams Consumer script and run it in the background
	  - API Request (pipeline_name, started_timestamp)
	  - HTTP 200 status response
	* - 14
	  - stop_publishing_delimitedfile():
	  - SSH into the Server & Kills the Kafka Steams Consumer script
	  - API Request (pipeline_name)
	  - HTTP 200 status response
	* - 15
	  - create_pipeline_iotgateway():
	  - Creates a new pipeline in the system pipelines index with default status
	  - API Request (pipeline_name, created_timestamp, pipeline_source)
	  - HTTP 200 status response
	* - 16
	  - start_publishing_iotgateway():
	  - SSH into the Producer & Consumer server & Invokes the Python Producer & Kafka Steams Consumer scripts and run it in the background
	  - API Request (pipeline_name, started_timestamp)
	  - HTTP 200 status response
	* - 17
	  - stop_publishing_iotgateway():
	  - SSH into the Producer & Consumer server & Kills the Python Producer & Kafka Steams Consumer scripts. Creates the Kafka topic if not exists.
	  - API Request (pipeline_name)
	  - HTTP 200 status response
	* - 18
	  - create_pipeline_kepware():
	  - Creates a new pipeline in the system pipelines index with default status
	  - API Request (pipeline_name, created_timestamp, pipeline_source)
	  - HTTP 200 status response
	* - 19
	  - start_publishing_kepware():
	  - Starts the Producer as subprocess, SSH into the Consumer server & Invokes the Kafka Steams Consumer scripts and run it in the background. Creates the Kafka topic if not exists.
	  - API Request (pipeline_name, started_timestamp)
	  - HTTP 200 status response
	* - 20
	  - stop_publishing_kepware():
	  - Kills the Producer using subpreocess, SSH into the Consumer server & Kills the Kafka Steams Consumer scripts
	  - API Request (pipeline_name)
	  - HTTP 200 status response
	* - 
	  - Dynamic Mapper
	  - 
	  - 
	  - 
	* - 21
	  - get_message_key()
	  - It check the input file for a key that contains string value whose length is greater than 25.
	  - Takes the dictionary object as input
	  - Gives a tuple with key in the 0th index and value at the 1st index . Ex: ('message', 'Cambium100113500,1554141120000,5,ok,5,5,4,ok,6,10,4,4,-58,ok,-75,Crititcal.Threshold,-58,-58,0.02,ok,0.02,0.02,5865,ok,5865,5865,31.284,warning,20,41,31.284,31.284,0,up,10,20,0,0,233,ok,233,233,100,ok,80,60,100,100,normal,ok,normal,normal,3,ok,6,10,3,3,-62,ok,-75,-80,-62,-62,0.01,ok,0.01,0.01,1478085,ok,1478085,1478085,15.102,402,50,0.7,175.2496')
	* - 22
	  - get_type()
	  - It checks the datatype of each input "value" by using "literal_eval"
	  - Takes the dictionary object as input
	  - Gives data type as output which are int, float and str.
	* - 23
	  - get_timestamp_key()
	  - It check the input for a key that contains unix TimeStamp value, which has 10or 13 no of digits in it
	  - Takes the dictionary object as input
	  - Gives a tuple with key in the 0th index and value at the 1st index . Ex ('eventTimeStamp', '1554141120000')
	* - 24
	  - get_mapping()
	  - It takes each key,value pairs from the input file and maps it to the required datatype based on the given conditions
	  - Takes the dictionary object as input
	  - Gives out json object with keys and values mapped with the required datatypes and additional information


.. toctree::
