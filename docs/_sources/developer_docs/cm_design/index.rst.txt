#########
CM design
#########


********************
Retention properties
********************

.. raw:: html

	<p>These can be changed by accessing the server.properties file in the Kafka/config folder</p>


The following configurations control the disposal of log segments. The policy can be set to delete segments after some time, or after a given size has accumulated. A segment will be deleted whenever either of these criteria is met. Deletion always happens from the end of the log. The minimum age of a log file to be eligible for deletion due to age
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


log.retention.hours=168
-----------------------


A size-based retention policy for logs. Segments are pruned from the log unless the remaining segments drop below log.retention.bytes. Functions independently of log.retention.hours.
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


log.retention.bytes=1073741824
------------------------------


The maximum size of a log segment file. When this size is reached a new log segment will be created.
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


log.segment.bytes=1073741824
----------------------------


The interval at which log segments are checked to see if they can be deleted according to the retention policies
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


log.retention.check.interval.ms=300000
--------------------------------------

*****************
thread properties
*****************

.. raw:: html

	<p>These can be changed by accessing the server.properties file in the Kafka/config folder</p>


background.threads
------------------

.. raw:: html

	<p>The number of threads to use for various background processing tasks</p>


num.io.threads
--------------

.. raw:: html

	<p>The number of threads that the server uses for processing requests, which may include disk I/O</p>


num.network.threads
-------------------

.. raw:: html

	<p>The number of threads that the server uses for receiving requests from the network and sending responses to the network</p>


log.cleaner.threads
-------------------

.. raw:: html

	<p>The number of background threads to use for log cleaning</p>

********************
Rebalance properties
********************


auto.leader.rebalance.enable
----------------------------

.. raw:: html

	<p>Enables auto leader balancing. A background thread checks the distribution of partition leaders at regular intervals, configurable by `leader.imbalance.check.interval.seconds`. If the leader imbalance exceeds `leader.imbalance.per.broker.percentage`, leader rebalance to the preferred leader for partitions is triggered.</p>


group.initial.rebalance.delay.ms
--------------------------------

.. raw:: html

	<p>The amount of time the group coordinator will wait for more consumers to join a new group before performing the first rebalance. A longer delay means potentially fewer rebalances but increases the time until processing begins.</p>


rebalance.timeout.ms
--------------------

.. raw:: html

	<p>The maximum allowed time for each worker to join the group once a rebalance has begun. This is basically a limit on the amount of time needed for all tasks to flush any pending data and commit offsets. If the timeout is exceeded, then the worker will be removed from the group, which will cause offset commit failures.</p>

**************
Log properties
**************


log.flush.interval.messages
---------------------------

.. raw:: html

	<p>The number of messages accumulated on a log partition before messages are flushed to disk</p>


log.flush.interval.ms
---------------------

.. raw:: html

	<p>The maximum time in ms that a message in any topic is kept in memory before flushed to disk. If not set, the value in log.flush.scheduler.interval.ms is used</p>


log.flush.start.offset.checkpoint.interval.ms
---------------------------------------------

.. raw:: html

	<p>The frequency with which we update the persistent record of the last flush which acts as the log recovery point</p>


log.flush.scheduler.interval.ms
-------------------------------

.. raw:: html

	<p>The frequency in ms that the log flusher checks whether any log needs to be flushed to disk</p>

.. raw:: html

	<p><br/><a href="https://kafka.apache.org/documentation/#brokerconfigs_log.flush.scheduler.interval.ms">log.flush.scheduler.interval.ms</a></p>

.. raw:: html

	<p>The frequency in ms that the log flusher checks whether any log needs to be flushed to disk</p>


log.flush.start.offset.checkpoint.interval.ms
---------------------------------------------

.. raw:: html

	<p>The frequency with which we update the persistent record of log start offset</p>

*******************
Consumer properties
*******************

.. raw:: html

	<p><code>max.poll.records (default=500)</code> defines the maximum number of messages that a consumer can poll at once.</p>

.. raw:: html

	<p><code>max.partition.fetch.bytes (default=1048576)</code> defines the maximum number of bytes that the server returns in a poll for a single partition.</p>

.. raw:: html

	<p><code>max.poll.interval.ms (default=300000)</code> defines the time a consumer has to process all messages from a poll and fetch a new poll afterward. If this interval is exceeded, the consumer leaves the consumer group.</p>

.. raw:: html

	<p><code>heartbeat.interval.ms (default=3000)</code> defines the frequency with which a consumer sends heartbeats.</p>

.. raw:: html

	<p><code>session.timeout.ms (default=10000)</code> defines the time a consumer has to send a heartbeat. If no heartbeat was received in that timeout, the member is considered dead and leaves the group.</p>


log.flush.start.offset.checkpoint.interval.ms
---------------------------------------------

.. raw:: html

	<p>The frequency with which we update the persistent record of log start offset</p>

********************
Rebalance properties
********************


auto.leader.rebalance.enable
----------------------------

.. raw:: html

	<p>Enables auto leader balancing. A background thread checks the distribution of partition leaders at regular intervals, configurable by `leader.imbalance.check.interval.seconds`. If the leader imbalance exceeds `leader.imbalance.per.broker.percentage`, leader rebalance to the preferred leader for partitions is triggered.</p>


group.initial.rebalance.delay.ms
--------------------------------

.. raw:: html

	<p>The amount of time the group coordinator will wait for more consumers to join a new group before performing the first rebalance. A longer delay means potentially fewer rebalances, but increases the time until processing begins.</p>


session.timeout.ms
------------------

.. raw:: html

	<p>The timeout used to detect client failures when using Kafka's group management facility. The client sends periodic heartbeats to indicate its liveness to the broker. If no heartbeats are received by the broker before the expiration of this session timeout, then the broker will remove this client from the group and initiate a rebalance. Note that the value must be in the allowable range as configured in the broker configuration by <code>group.min.session.timeout.ms</code> and <code>group.max.session.timeout.ms</code></p>


rebalance.timeout.ms
--------------------

.. raw:: html

	<p>The maximum allowed time for each worker to join the group once a rebalance has begun. This is basically a limit on the amount of time needed for all tasks to flush any pending data and commit offsets. If the timeout is exceeded, then the worker will be removed from the group, which will cause offset commit failures.</p>


acceptable.recovery.lag
-----------------------

.. raw:: html

	<p>The maximum acceptable lag (number of offsets to catch up) for a client to be considered caught-up enough to receive an active task assignment. Upon assignment, it will still restore the rest of the changelog before processing. To avoid a pause in processing during rebalances, this config should correspond to a recovery time of well under a minute for a given workload. Must be at least 0.</p>


probing.rebalance.interval.ms
-----------------------------

.. raw:: html

	<p>The maximum time in milliseconds to wait before triggering a rebalance to probe for warmup replicas that have finished warming up and are ready to become active. Probing rebalances will continue to be triggered until the assignment is balanced. Must be at least 1 minute.</p>

.. toctree::
