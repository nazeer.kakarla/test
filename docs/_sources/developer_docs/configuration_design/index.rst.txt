####################
Configuration design
####################


.. toctree::

	data_pipelines_design/index
	integrations/index
	kafka_design/index
	ssl_certificate/index