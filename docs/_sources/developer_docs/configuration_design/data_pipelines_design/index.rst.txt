#####################
Data Pipelines design
#####################


.. raw:: html

	<p>Sites &gt; Plants &gt; Functions &gt; Assets &gt; Watchers &gt; Devices</p>

.. raw:: html

	<p>Pipelines <em><span style="color: rgb(151,160,175);">contain</span></em> Devices</p>

.. raw:: html

	<p>Devices <span style="color: rgb(151,160,175);">are mapped to</span> Monitoring Locations <span style="color: rgb(151,160,175);">that are attached to</span> Asset (An Asset can have multiple MLocs)</p>



.. raw:: html

	<p>Pipelines</p>



.. raw:: html

	<p>Used to store all the configuration data (metadata)</p>



.. raw:: html

	<p>What is automated, and what is still manual.</p>



*****************
Data Source Types
*****************


Edge Server
===========


Process data
------------

.. list-table::
	:header-rows: 1

	* - Level 1 >>
	  - Level 2/3 >> (Edge Server)
	  - 4PointX Platform
	* - PLC Tags (OPC Server made available by customer - Mitsubishi, Siemens vendor)
	  - Machinebeat > Logstash
	  - Kafka
	* - <Customer’s own software>
	  - CSV (in FTP) > Logstash (transformation)
	  - Kafka


.. raw:: html

	<p style="margin-left: 30.0px;">Data sent on Ethernet (TCP/IP &gt; Node-Red &gt; MQTT &gt; Kafka &gt; ES)</p>

.. raw:: html

	<p style="margin-left: 30.0px;">Energy Meter (LoRA Node &gt; LoRA Gateway &gt; <span style="color: rgb(151,160,175);">Edge Server &gt; Node-Red &gt;</span> MQTT &gt; Kafka &gt; ES)</p>

.. raw:: html

	<p style="margin-left: 30.0px;">Condition Data &gt; </p>


IU Cloud
--------

.. list-table::
	:header-rows: 1

	* - IU Cloud
	  - 4PointX Platform
	  - 4PointX Platform
	* - APIs
	  - Python producer
	  - Elastic


.. raw:: html

	<p>GCS Bucket</p>

.. toctree::
