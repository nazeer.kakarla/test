#######################
Updating WhatsApp Token
#######################


.. raw:: html

	<p>Steps to updating WhatsApp Token:</p>

.. raw:: html

	<ul><li><p>Fetch(Copy) the latest token from the <a href="https://developers.facebook.com/apps/887175339198235/whatsapp-business/wa-dev-console/?business_id=1384728672168696">meta-app site</a> (ask Kiran for creds)</p></li><li><p>Do SSH into the test VM (steps are shown in <a href="https://everlytics.atlassian.net/wiki/spaces/DOCS/pages/8409185354/Environments#">Environments</a> → <a href="https://everlytics.atlassian.net/wiki/spaces/DOCS/pages/9345695745/Test#">Test</a>)</p></li><li><p>Go into the Docker consumer</p><ac:structured-macro ac:macro-id="e7620e81-be74-4d72-a37a-a6351a9c221c" ac:name="code" ac:schema-version="1"><ac:plain-text-body><![CDATA[docker exec -it <id> bash]]></ac:plain-text-body></ac:structured-macro></li><li><p>Install nano.<br/>use this command : </p><ac:structured-macro ac:macro-id="7b8755ef-9662-4d67-b8e0-772a3eaf948a" ac:name="code" ac:schema-version="1"><ac:plain-text-body><![CDATA[apt update && apt install nano]]></ac:plain-text-body></ac:structured-macro></li><li><p>Open and Update WhatsApp creds:</p><ul><li><p>Go to the config directory</p></li><li><p>Do: </p><ac:structured-macro ac:macro-id="5b52ac64-3dd8-476e-8b6d-187d5438a7f7" ac:name="code" ac:schema-version="1"><ac:plain-text-body><![CDATA[nano whatsapp_credentials.json]]></ac:plain-text-body></ac:structured-macro></li><li><p>update the <code>authorization</code><br/>Eg: </p><ac:structured-macro ac:macro-id="b2d0a8df-9fcc-4257-8ee4-2c7863d02c68" ac:name="code" ac:schema-version="1"><ac:plain-text-body><![CDATA[{
    "whatsapp": {
        "phone_number_id": 194908467029998,
        "authorization": "Bearer EAAMm4ZAryRxsBO263ZBPmlKowZBidyJZByRdYrozLZC2OuWLjK42ZATM2VkV4IBbTxoxPcxAlfMZBt5ZACIL57yWTqQMZAlnDNydiKt66Cdj3pMPMZBJpQbwQBAwlBKoOpQMD3zRZCnbJF7V2rFeG1io6HFuAfsftZAySla788MfZAZArwS8hLe4Vnmo4mP7e9wZCXXEmZACzOSRykcRwgCu77BFpUpOWs0C2lDzNwZDZD"
    }
}
]]></ac:plain-text-body></ac:structured-macro></li><li><p>Use Bearer token (add “Bearer” before token)</p></li></ul></li><li><p>Save the changes</p></li><li><p>Kill the “<u>alerts_consumer.py</u>“<br/>use : </p><ac:structured-macro ac:macro-id="4339b72b-4491-4e21-aa56-5d6e353b185c" ac:name="code" ac:schema-version="1"><ac:plain-text-body><![CDATA[kill -9 <process id>]]></ac:plain-text-body></ac:structured-macro></li><li><p>Run the “alert_consumer.py” script or “run_alerts_consumer.sh”</p></li><li><p>close the terminal / ctrl +D for exit from the docker <br/><br/></p></li></ul>

.. toctree::
