########################
Documentation formatting
########################




.. list-table::
	:header-rows: 1

	* - POST /_config/assets/


.. raw:: html

	<p><strong>Description</strong> This is used for creating new asset.</p>

.. raw:: html

	<p><strong>HTTP Method</strong> POST</p>

.. raw:: html

	<p><strong>Endpoint:</strong> <code>/_config/assets/</code></p>

.. raw:: html

	<p><strong>Request Header</strong> <code>Authorization</code> Basic &lt;token&gt;</p>

.. raw:: html

	<p><strong>Request Parameters</strong></p>

.. raw:: html

	<p>Body Parameters</p>

.. raw:: html

	<ul><li><p><code>asset_name</code> (string, <em>required</em>): The name of the asset.</p></li><li><p><code>function_id</code> (string, <em>required</em>): The ID of the associated function.</p></li><li><p><code>plant_id</code> (string, <em>optional</em>): The ID of the associated plant (default is determined from the function).</p></li><li><p><code>site_id</code> (string, <em>optional</em>): The ID of the associated site (default is determined from the plant).</p></li><li><p><code>asset_category</code> (string, <em>required</em>): The category of the asset (e.g., “Rotating” or “Stationary”).</p></li><li><p><code>is_critical</code> (string, <em>required</em>): Indicates whether the asset is critical (“yes” or “no”).</p></li><li><p><code>attributes</code> (object, <em>required</em>): Additional attributes specific to the asset.</p></li><li><p><code>machine_type</code> (string, <em>optional</em>): The type or category of the machine.</p></li><li><p><code>rotating_speed</code> (number, <em>optional</em>): The rotating speed of the asset, applicable for rotating machinery (e.g., RPM for a motor).</p></li><li><p><code>manufacturer</code> (string, <em>optional</em>): The manufacturer or company that produced the asset.</p></li><li><p><code>machine_model</code> (string, <em>optional</em>): The model or version of the machine.</p></li><li><p><code>serial_no</code> (string, <em>optional</em>): Serial or unique identifier for the asset.</p></li><li><p><code>phases</code> (string, <em>optional</em>): The number of phases the asset operates in.</p></li><li><p><code>kw</code> (number, <em>optional</em>): The power rating of the asset in kilowatts (kW).</p></li><li><p><code>frequency</code> (number, <em>optional</em>): The frequency at which the asset operates (in Hertz, Hz).</p></li><li><p><code>volts</code> (number, <em>optional</em>): The voltage rating of the asset (in volts, V).</p></li><li><p><code>amp</code> (number, <em>optional</em>): The current rating of the asset in amperes (Amps, A).</p></li></ul>

.. raw:: html

	<p><strong>Sample Request</strong></p>

.. list-table::
	:header-rows: 1

	* -     curl --location 'https://test.4pointx.com/_config/assets/' \   A--header  'Content-Type: application/json' \ --header  'Authorization: Basic YWRtaW46dDc1bDh5Y0JoT05tblJNOWpiaHdqVWhkUTU1ZlZvMnU=' \ --data '{"asset_name": "demo-asset2-test", "asset_category": "All", "function_id": "45c98f40-75e3-49e7-b178-d25ec878db79", "is_critical": "no", "attributes": { "machine_type": "motor", "rotating_speed": 0, "manufacturer": "", "serial_no": "", "phases": "single-phase", "kw": 0, "frequency": 0, "volts": 0, "amp": 0 } }'
	* - 


.. list-table::
	:header-rows: 1

	* - POST /_config/assets


.. toctree::
